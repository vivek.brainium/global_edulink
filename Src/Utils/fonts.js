const Font = {
    micro_size: 14,
    small_size: 16,
    medium_size: 18,
    large_size: 20,
    extra_large:24
  };
  
  export default Font;