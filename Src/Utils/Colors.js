const COLORS = {
    white: '#fff',
    black: '#000',
    statusBarBgColor: "#0776a6",
    headerColor: "#017cb2",
    grey: "grey",
    textBoader: "#7a868a",
    forgotTextColor: "#6e7578",
    getStart: "#00b0dc",
    educationColor: "#1a5086",
    tabColor: "#044f6f",
    bottonColor: "#eeb427"
}

export default COLORS;
