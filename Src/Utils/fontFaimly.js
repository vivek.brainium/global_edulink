const FontFaimly = {
    Raleway_Bold: "Raleway-Bold",
    Raleway_Medium: "Raleway-Medium",
    Raleway_Light: "Raleway-Light",
    Raleway_Regular: "Raleway-Regular",
    Raleway_SemiBold: "Raleway-SemiBold",
    Muli_SemiBold: "Muli-SemiBold",
    Muli_Bold: "Muli-Bold",
    Muli_Light: "Muli Light",
    
  };
  
  export default FontFaimly;