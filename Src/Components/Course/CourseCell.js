

import React, { useEffect, useState } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Image,
    Dimensions,
    ImageBackground,
    TouchableOpacity
} from 'react-native';
import Colors from "./../../Utils/Colors";
const screenHeight = Dimensions.get('window').height;
const screenWidth = Dimensions.get('window').width;
import fontComponent from "./../../Utils/fonts"
import fontFamilyComponent from "./../../Utils/fontFaimly"
const homeIcon = require("./../../Images/Home.png")
const searchIcon = require("./../../Images/Search.png")
const Images1 = require("./../../Images/Testing/image-1.png")
import Entypo from 'react-native-vector-icons/Entypo';
const HeartIcon = require("./../../Images/Heart.png")
const HeartIcon1 = require("./../../Images/Heart_like.png")
const ListcheckIcon = require("./../../Images/list-check.png")
const ListUncheckIcon = require("./../../Images/list-uncheck.png")


const CourseCell = (props) => {


    return (
        <View style={{ marginHorizontal: 20, marginVertical: 10, flexDirection: "row", backgroundColor: Colors.white }}>
            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                {
                    props.data.map(() => {
                        return (

                            <View style={{ backgroundColor: "#044e6f", borderRadius: 5, marginRight: 10 }}>
                                <Text style={{ color: Colors.white, fontFamily: fontFamilyComponent.Raleway_Medium, fontSize: fontComponent.small_size, padding: 10 }}>Course 1</Text>
                            </View>

                        )
                    })
                }
            </ScrollView>
        </View>
    )
}

export default CourseCell;