

import React, { useEffect, useState } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Image,
    Dimensions,
    ImageBackground,
    TouchableOpacity
} from 'react-native';
import Colors from "./../../Utils/Colors";
const screenHeight = Dimensions.get('window').height;
const screenWidth = Dimensions.get('window').width;
import fontComponent from "./../../Utils/fonts"
import fontFamilyComponent from "./../../Utils/fontFaimly"
const homeIcon = require("./../../Images/Home.png")
const searchIcon = require("./../../Images/Search.png")
const Images1 = require("./../../Images/Testing/image-1.png")
import Entypo from 'react-native-vector-icons/Entypo';
const HeartIcon = require("./../../Images/Heart.png")
const HeartIcon1 = require("./../../Images/Heart_like.png")
const ListcheckIcon = require("./../../Images/list-check.png")
const ListUncheckIcon = require("./../../Images/list-uncheck.png")


const CourseListCell = (props) => {
    const [rating, setRating] = useState([1, 2, 3, 4, 5])
    const [heartFlag, setHeartFlag] = useState(false)
    const [listFlag, setListFlag] = useState(false)


    const handleChangeHeart = () => {
        if (heartFlag) {
            setHeartFlag(false)
        } else {
            setHeartFlag(true)
        }
    }

    const handleOnchangeList = () => {
        if (listFlag) {
            setListFlag(false)
        } else {
            setListFlag(true)
        }
    }

    return (

        <View>
            <TouchableOpacity style={{ flexDirection: "row", paddingVertical: 10 }}
                activeOpacity={0.6}
                onPress={()=> props.handleCourseList()}
            >

                <View>
                    <Image source={Images1} style={{ width: screenWidth / 3, height: screenHeight / 6 }} />
                </View>
                {/* <View style={{ }}> */}
                <View style={{ paddingLeft: 10, flex: 1 }}>

                    <View style={{ flexDirection: "row", }}>

                        <View style={{ flex: 0.99 }}>
                            <Text numberOfLines={1} style={{ color: "#040f14", fontSize: fontFamilyComponent.Raleway_Medium, fontSize: fontComponent.small_size }}>PRINCE2@ 2017</Text>
                            <Text numberOfLines={1} style={{ color: "#040f14", fontSize: fontFamilyComponent.Raleway_Medium, fontSize: fontComponent.small_size }}>Foundation Course</Text>
                        </View>

                        <View style={{ flexDirection: "row" }}>
                            <TouchableOpacity activeOpacity={0.6} onPress={() => handleChangeHeart()}>
                                {heartFlag == false ?

                                    <Image source={HeartIcon} style={{ width: 23, height: 20 }} />

                                    :

                                    <Image source={HeartIcon1} style={{ width: 23, height: 20 }} />

                                }
                            </TouchableOpacity>

                            <TouchableOpacity style={{ paddingLeft: 8 }} activeOpacity={0.6} onPress={() => handleOnchangeList()}>
                                {listFlag == false ?

                                    <Image source={ListUncheckIcon} style={{ width: 20, height: 20 }} />

                                    :

                                    <Image source={ListcheckIcon} style={{ width: 20, height: 20 }} />

                                }
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={{ flexDirection: "row", paddingVertical: 5, alignItems: "center" }}>
                        {rating.map(() => {
                            return (
                                <Entypo
                                    name="star"
                                    size={20}
                                    color={"#eeb327"}
                                />
                            )
                        })}
                        <Text style={{ textAlign: "center", fontFamily: fontFamilyComponent.Muli_SemiBold, fontSize: fontComponent.medium_size, paddingLeft: 5, color: "#686e71" }}>4.9</Text>
                    </View>


                    <View style={{ flexDirection: "row", alignItems: "flex-end" }}>
                        <Text style={{ color: "#a6a6a6", textAlign: "justify", fontSize: 12, fontFamily: fontFamilyComponent.Muli_SemiBold }}>$159</Text>
                        <Text style={{ color: "#125e9a", fontSize: fontComponent.small_size, fontFamily: fontFamilyComponent.Muli_Bold, paddingHorizontal: 1 }}>$159</Text>
                        <Text style={{ fontFamily: fontFamilyComponent.Muli_SemiBold, fontSize: 12 }}>(<Text style={{ color: "#051c2e", fontFamily: fontFamilyComponent.Muli_SemiBold }}>inc</Text><Text style={{ fontFamily: fontFamilyComponent.Muli_SemiBold }}>. VAT)</Text></Text>
                        <TouchableOpacity
                            style={{ backgroundColor: "#eeb427", justifyContent: "center", alignItems: "center", borderRadius: 50, marginLeft: 5 }}
                        >
                            <Text style={{ fontSize: fontComponent.micro_size, paddingHorizontal: 10, paddingVertical: 3, textAlign: "center", fontFamily: fontFamilyComponent.Raleway_Medium, color: Colors.white }}>Buy Now</Text>
                        </TouchableOpacity>
                    </View>
                </View>


            </TouchableOpacity>

            <View style={{ height: 0.5, backgroundColor: Colors.grey, }} />
        </View>


        // <View style={{ flex: 1 }}>
        //     <View style={{ flexDirection: "row", paddingVertical: 20, borderBottomWidth: 0.8, borderBottomColor: Colors.grey }}>
        //         <View style={{ height: screenHeight / 6, width: screenWidth / 3 }}>
        //             <Image source={Images1}
        //                 style={{ height: "100%", width: "100%" }}
        //             />
        //         </View>
        //         <View style={{ paddingLeft: 15, flex: 1 }}>

        //             <View>
        //                 <Text numberOfLines={1} style={{ fontFamily: fontFamilyComponent.Raleway_Medium, fontSize: fontComponent.small_size }}>PRINCE2® 2017 Foundation Course</Text>
        //                 <Text numberOfLines={1} style={{ fontFamily: fontFamilyComponent.Raleway_Medium, fontSize: fontComponent.small_size }}>Brainium</Text>
        //             </View>

        //             <View style={{ flexDirection: "row", paddingVertical: 8 }}>

        //                 {rating.map(() => {
        //                     return (
        //                         <Entypo
        //                             name="star"
        //                             size={20}
        //                             color={"#eeb327"}
        //                         />
        //                     )
        //                 })}

        //             </View>
        //             <View style={{ backgroundColor: "#eeb327", width: "35%", borderRadius: 50, justifyContent: "center", alignItems: "center" }}>
        //                 <Text style={{ color: Colors.white, paddingVertical: 3, fontSize: fontComponent.small_size, fontFamily: fontFamilyComponent.Raleway_Medium, textAlign: "center" }}>View</Text>
        //             </View>

        //         </View>
        //     </View>
        //     <View style={{ height: 1, color: Colors.black }} />
        // </View>
    )
}

export default CourseListCell;