
import React from 'react';
import {
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Image,
    Dimensions,
    TouchableOpacity,
    ImageBackground
} from 'react-native';
import Colors from "../../Utils/Colors";
const screenHeight = Dimensions.get('window').height;
const screenWidth = Dimensions.get('window').width;
import fontComponent from "../../Utils/fonts";
import fontFamilyComponent from "../../Utils/fontFaimly";

const product01Icon = require("./../../Images/Product/product01.png")
const product02Icon = require("../../Images/Product/product02.png")
const product03Icon = require("./../../Images/Product/product03.png")
const product04Icon = require("./../../Images/Product/product04.png")
const logoIcon = require("./../../Images/Product/logo.png")

const ProductListComponent = (props) => {
    return (

        <View style={{ alignItems: "center" }}>
            <View style={{ position: "relative" }}>
                <Image source={props.item.image} style={{ height: screenHeight / 2.5 }} />
            </View>
            <View style={{ position: "absolute", paddingTop: 10, paddingHorizontal: 10 }}>
                <View style={{ alignItems: "center" }}>
                    <Image source={logoIcon} style={{ width: 40, height: 40 }} />
                </View>

                <Text style={{ textAlign: "center", color: Colors.white, fontFamily: fontFamilyComponent.Raleway_Medium, paddingTop: 5, fontSize: fontComponent.small_size }}>Level 3 Award in Education & training</Text>

                <Text style={{ textAlign: "center", color: Colors.white, fontFamily: fontFamilyComponent.Muli_SemiBold, fontSize: fontComponent.small_size }}>$999</Text>

                <View style={{ backgroundColor: "#53c4ff", paddingHorizontal: 7, paddingTop: 5, width: screenWidth / 2.37, height: screenHeight / 4.76, marginTop: 10, borderTopRightRadius: 10, borderTopLeftRadius: 10 }}>

                    <View style={{ flexDirection: "row", alignItems: "center" }}>
                        <View style={{ width: 8, height: 8, borderColor: Colors.white, borderWidth: 2, borderRadius: 4 }} />
                        <Text style={{ paddingLeft: 5, color: Colors.white, fontSize: 12, textAlign: "center" }}>3 days</Text>
                    </View>

                    <View style={{ flexDirection: "row", alignItems: "center", paddingTop: 5 }}>
                        <View style={{ width: 8, height: 8, borderColor: Colors.white, borderWidth: 2, borderRadius: 4 }} />
                        <Text style={{ paddingLeft: 5, color: Colors.white, fontSize: 12 }}>Certificate of Completion Awarded by Pearson (Edexcel)</Text>
                    </View>


                    <View style={{ flexDirection: "row", alignItems: "center", paddingTop: 5 }}>
                        <View style={{ width: 8, height: 8, borderColor: Colors.white, borderWidth: 2, borderRadius: 4 }} />
                        <Text style={{ paddingLeft: 5, color: Colors.white, fontSize: 12, textAlign: "center" }}>Exam Includes</Text>
                    </View>

                    <View style={{ paddingTop: 15 }}>
                        <TouchableOpacity activeOpacity={.6} style={{ backgroundColor: "#eeb427", borderRadius: 50, alignItems: "center" }}>
                            <Text style={{ color: Colors.white, fontSize: fontComponent.micro_size, padding: 4, fontFamily: fontFamilyComponent.Raleway_Medium }}>Appy Now</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>

        </View>

    );
};

const styles = StyleSheet.create({
    container: { backgroundColor: Colors.white, flex: 1 },
    imageLogo: { width: 120, height: 120, borderRadius: 60 },
    logoContainer: { flex: 0.5, justifyContent: "center", alignItems: "center" },
});

export default ProductListComponent;