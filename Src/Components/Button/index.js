import React from "react"
import {
    Text,
    View,
    StyleSheet,
    Dimensions,
    ImageBackground,
    TouchableOpacity
} from "react-native";
const ButtonImage = require("./../../Images/Button.png")
import fontComponent from "./../../Utils/fonts"
import fontFamilyComponent from "./../../Utils/fontFaimly"
const screenHeight = Dimensions.get('window').height;
const screenWidth = Dimensions.get('window').width;
import Colors from "./../../Utils/Colors";


const BottunComponent = (props) => {
    return (
        <View style={{}}>
            <TouchableOpacity
                activeOpacity={.6}
            >
                <ImageBackground source={ButtonImage}
                    style={props.imageStyle}
                >
                    <View style={styles.buttomStyle}>
                        <Text style={styles.buttomText}>{props.buttomTitle}</Text>
                    </View>
                </ImageBackground>
            </TouchableOpacity>
        </View>
    )
}

export default BottunComponent;


const styles = StyleSheet.create({
    container: {

    },
    buttomText: { color: Colors.white, fontSize: fontComponent.large_size, fontFamily: fontFamilyComponent.Raleway_Medium },
    buttomImage: { height: screenHeight / 12, width: screenWidth / 1.9 },
    buttomStyle: { justifyContent: "center", alignItems: "center", height: screenHeight / 11.5 },
});