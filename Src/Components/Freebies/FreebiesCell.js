
import React, { Component } from 'react';
import {
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Image,
    Dimensions,
    TouchableOpacity,
    ImageBackground
} from 'react-native';
import HeaderScreen from "./../../Components/Header";
import Colors from "./../../Utils/Colors";
const screenHeight = Dimensions.get('window').height;
const screenWidth = Dimensions.get('window').width;
import fontComponent from "./../../Utils/fonts";
import fontFamilyComponent from "./../../Utils/fontFaimly";
const ArrowIcon = require("./../../Images/Arrow.png")
const FreebiesIcon = require("./../../Images/Testing/freebis.png")
const FreebiesIcon01 = require("./../../Images/Testing/freebis01.png")
const FreebiesIcon02 = require("./../../Images/Testing/freebis02.png")
const FreebiesIcon03 = require("./../../Images/Testing/freebis03.png")


const FreebiesCell = () => {

    return (

        <View style={styles.container}>
            <Image source={FreebiesIcon} style={styles.image_container} />

            <Text style={styles.text_container}>Certificate in COVID-19 (Coronavirus) Awareness</Text>

            <View style={styles.botton}>
                <TouchableOpacity style={styles.subBotton}>
                    <Text style={styles.botton_text}>Start Now</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
};

const styles = StyleSheet.create({
    container: { width: screenWidth / 2.36, marginRight: 20, marginTop: 20 },
    image_container: { width: screenWidth / 2.36, height: screenHeight / 6.5 },
    text_container: { textAlign: "center", paddingTop: 10, fontFamily: fontFamilyComponent.Raleway_SemiBold, color: Colors.black },
    botton: { paddingTop: 15, alignItems: "center" },
    subBotton: { backgroundColor: Colors.bottonColor, width: screenWidth / 3, alignItems: "center", borderRadius: 50 },
    botton_text: { color: Colors.white, fontFamily: fontFamilyComponent.Raleway_SemiBold, paddingVertical: 10, fontSize: fontComponent.medium_size }
});

export default FreebiesCell;