
import React, { useEffect, useState } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Image,
    Dimensions,
    ImageBackground,
    TouchableOpacity
} from 'react-native';
import Colors from "./../../Utils/Colors";
const screenHeight = Dimensions.get('window').height;
const screenWidth = Dimensions.get('window').width;
import fontComponent from "./../../Utils/fonts"
import fontFamilyComponent from "./../../Utils/fontFaimly"


const TabView = (props) => {
    return (
        <View style={{ height: screenHeight / 12, backgroundColor: Colors.headerColor, borderBottomLeftRadius: 30, borderBottomRightRadius: 30, justifyContent: "center" }}>
            <View style={{ flexDirection: "row", marginHorizontal: 20 }}>

                <TouchableOpacity
                    activeOpacity={0.6}
                    style={{ backgroundColor: props.tabBackgroundColor, borderRadius: 50 }}
                    onPress={() => props.handleTabView("1")}
                >
                    <Text style={{ color: Colors.white, paddingVertical: 7, paddingHorizontal: 17, textAlign: "center", fontSize: fontComponent.small_size }}>Courses</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    activeOpacity={0.6}
                    style={{ backgroundColor: props.tabBackgroundColor1, borderRadius: 50 }}
                    onPress={() => props.handleTabView("2")}
                >
                    <Text style={{ color: Colors.white, paddingVertical: 7, paddingHorizontal: 17, textAlign: "center", fontSize: fontComponent.small_size }}>Products</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    activeOpacity={0.6}
                    style={{ backgroundColor: props.tabBackgroundColor2, borderRadius: 50 }}
                    onPress={() => props.handleTabView("3")}
                >
                    <Text style={{ color: Colors.white, paddingVertical: 7, paddingHorizontal: 17, textAlign: "center", fontSize: fontComponent.small_size }}>Career</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    activeOpacity={0.6}
                    style={{ backgroundColor: props.tabBackgroundColor3, borderRadius: 50 }}
                    onPress={() => props.handleTabView("4")}
                >
                    <Text style={{ color: Colors.white, paddingVertical: 7, paddingHorizontal: 17, textAlign: "center", fontSize: fontComponent.small_size }}>Freebies</Text>
                </TouchableOpacity>

            </View>
        </View>
    )
}

export default TabView;