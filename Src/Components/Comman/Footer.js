

import React, { useEffect, useState } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Image,
    Dimensions,
    ImageBackground,
    TouchableOpacity
} from 'react-native';
import Colors from "./../../Utils/Colors";
const screenHeight = Dimensions.get('window').height;
const screenWidth = Dimensions.get('window').width;
import fontComponent from "./../../Utils/fonts"
import fontFamilyComponent from "./../../Utils/fontFaimly"
const homeIcon = require("./../../Images/Home.png")
const searchIcon = require("./../../Images/Search.png")
const cardIcon = require("./../../Images/Cart.png")
const MessageIcon = require("./../../Images/Message.png")

const Footer = (props) => {
    return (
        <View style={{
            width: screenWidth,
            position: "absolute",
            bottom: 0,
            justifyContent: "center",
            backgroundColor: Colors.white
        }}>
            <View style={{
                height: 60,
                flexDirection: "row", shadowOffset: { width: 10, height: 10 }, borderTopWidth: 0,
                shadowColor: 'black',
                shadowOpacity: 1,
                elevation: 3,
                zIndex: 999,
                alignItems: "center", justifyContent: "space-between", paddingHorizontal: 40
            }}>

                <TouchableOpacity
                    onPress={() => props.handleHome()}
                    style={{ alignItems: "center", paddingTop: 10 }}
                >
                    <Image source={homeIcon} style={{ width: 20, height: 20 }} />
                    <Text style={{ color: Colors.black, fontFamily: fontFamilyComponent.Raleway_Medium }}>Home</Text>
                </TouchableOpacity>

                <View style={{ paddingBottom: 30 }}>
                    <TouchableOpacity onPress={() => props.handleSearch()} activeOpacity={0.6} style={{ backgroundColor: "#1e104b", top: 0, alignItems: "center", justifyContent: "center", height: 70, width: 70, borderRadius: 35 }}>
                        <Image source={searchIcon} style={{ width: 20, height: 20 }} />
                        <Text style={{ color: Colors.white, fontSize: 12 }}>Search</Text>
                    </TouchableOpacity>
                </View>

                {/* {props.flagForMessage ?
                    <TouchableOpacity
                    onPress={() => props.handleCard()}
                    style={{ paddingTop: 10, alignItems: "center" }}
                >
                    <Image source={MessageIcon} style={{ width: 25, height: 25 }} />
                    <Text style={{ color: Colors.black,  fontFamily: fontFamilyComponent.Raleway_Medium }}>Message</Text>
                </TouchableOpacity> : null} */}


                <TouchableOpacity
                    onPress={() => props.handleCard()}
                    style={{ paddingTop: 10 }}
                >
                    <Image source={cardIcon} style={{ width: 20, height: 20 }} />
                    <Text style={{ color: Colors.black,  fontFamily: fontFamilyComponent.Raleway_Medium }}>Card</Text>
                </TouchableOpacity>

            </View>
        </View>
    )
}

export default Footer;