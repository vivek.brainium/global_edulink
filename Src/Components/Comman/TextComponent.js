import React from 'react';
import {
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Image,
    Platform,
} from 'react-native';
import Colors from "./../../Utils/Colors"
import fontComponents from "./../../Utils/fonts"
import { TouchableOpacity, TextInput } from 'react-native-gesture-handler';
import fontFamilyComponent from "./../../Utils/fontFaimly"

const userIcon = require("./../../Images/User.png")
const TextCopmomnents = (props) => {
    return (
        <View style={styles.container}>
            <View style={{ flexDirection: "row", alignItems: "center", }}>
                {props.icon ? <props.iconName
                    name={props.iconType}
                    color={props.iconColor}
                    size={props.size}
                    style={props.iconStyle}
                /> : <Image source={ userIcon } style={{ width: 22, height: 22 }}/>}

                <TextInput
                    style={{ height: 50, fontSize: fontComponents.medium_size, width: "84%" }}
                    placeholder={props.placeholder}
                    placeholderTextColor={"#849196"}
                    secureTextEntry={props.secureTextEntry}
                    onChangeText={(value) => props.onChangeText(value)}
                />
            </View>
            <TouchableOpacity 
                style={{ alignItems: "center", justifyContent: "center" }}
                onPress={()=> props.onPressRight()}
            >
                {props.iconRight && <props.iconNameRight
                    name={props.iconTypeRight}
                    color={props.iconColorRight}
                    size={props.sizeRight}
                    style={props.iconStyleRight}
                />}
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10,
        flexDirection: "row",
        borderColor: Colors.textBoader,
        borderRadius: 5,
        borderWidth: 0.5,
        justifyContent: "space-between"
    }
});
export default TextCopmomnents;