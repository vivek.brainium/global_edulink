import React from 'react';
import {
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Platform,
} from 'react-native';
import Colors from "./../../Utils/Colors"
import AntDesign from 'react-native-vector-icons/AntDesign';
import fontComponents from "./../../Utils/fonts"
import { TouchableOpacity } from 'react-native-gesture-handler';

const HeaderCopmomnents = (props) => {
  return (
    <View style={styles.container}>

      {/* Let Header */}
      <View style={{ flexDirection: "row", alignItems: "center", flex: 1 }}>
        <TouchableOpacity
          onPress={() => props.onPress()}
        >
          <AntDesign
            name="arrowleft"
            // name={props.iconName}
            size={28}
            color={Colors.white}
          />
        </TouchableOpacity>
        <Text style={{ fontSize: fontComponents.extra_large, color: Colors.white, paddingLeft: 20 }}>{props.headerLeft}</Text>
      </View>

      {/* <View style={{ justifyContent: "center", flex: 0.4 }}>
        <Text style={{ fontSize: fontComponents.medium_size, color: Colors.white, paddingLeft: 20 }}>Center</Text>
      </View>
      <View style={{ alignItems: "flex-end", flex: 0.2 }}>
        <Text style={{ fontSize: fontComponents.medium_size, color: Colors.white, paddingLeft: 20 }}>Right</Text>
      </View> */}
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    height: Platform.OS == "android" ? 55 : 55,
    backgroundColor: Colors.headerColor, //.......after provied
    paddingHorizontal: 20,
    alignItems: "center",
    flexDirection: "row"
  },
})

export default HeaderCopmomnents;









// import React from 'react';
// // import LinearGradient from 'react-native-linear-gradient';
// import {View, Image, Text, SafeAreaView, TouchableOpacity} from 'react-native';
// import Ionicons from 'react-native-vector-icons/dist/Ionicons';

// // export const HeaderBackground = props => {
// //   return (
// //     <LinearGradient
// //       colors={['#130147', '#2864ea']}
// //       style={{flex: 1}}
// //       start={{x: 0, y: 0}}
// //       end={{x: 1, y: 0}}>
// //       {/* <SafeAreaView style={{ flex: 0, backgroundColor: "#001b82" }} /> */}
// //     </LinearGradient>
// //   );
// // };

// export const HeaderRight = props => {
//   return (
//     <View>
//       <Ionicons
//         style={{paddingRight: 20}}
//         name="ios-menu"
//         size={30}
//         color="#fff"
//       />
//     </View>
//   );
// };

// export const HeaderLeft = (props, navigation) => {
//   return (
//     <TouchableOpacity onPress={() => props.LeftNavigation()}>
//       <Ionicons
//         style={{paddingLeft: 15}}
//         name="ios-arrow-round-back"
//         size={30}
//         color="#fff"
//       />
//     </TouchableOpacity>
//   );
// };


// import React from 'react';
// import {
//   View,
//   Text,
//   TouchableOpacity
// } from "react-native"