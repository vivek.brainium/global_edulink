import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
const Stack = createStackNavigator();
import SplashScreen from "./../Container/Splash";
import LoginComponent from "./../Container/Login";
import SignUpComponent from "./../Container/SignUp";
import GetStartSignUp from "./../Container/SignUp/getStartSignup";
import ForgotPassword from "./../Container/Forgot"
import HomeComponents from "./../Container/Home"
import CourseList from "../Container/CourseList/index"
import CourseDetails from "./../Container/CourseList/CoursesDetails"
import FavouriteComponent from "./../Container/Favourite";
import ClassroomCourseType from "./../Container/Products/ClassroomCoursesType"
import ForumComponent from "./../Container/Forum";
import OnlineCoursesComponent from "./../Container/CourseList/OnlineCourses"
import PracticeLabComponent from "./../Container/Practice Lab/index";
import ProfilesComponent from "./../Container/Profiles/index"
import LiveOnlineClassComponent from "./../Container/Live Class/index"

const AuthScreen = () => {
    return (
        <Stack.Navigator
            screenOptions={{ headerShown: false }}
            initialRouteName={"SplashScreen"}
        >
            <Stack.Screen name="LiveOnlineClassComponent" component={LiveOnlineClassComponent} />
            <Stack.Screen name="ProfilesComponent" component={ProfilesComponent} />
            <Stack.Screen name="PracticeLabComponent" component={PracticeLabComponent} />
            <Stack.Screen name="OnlineCoursesComponent" component={OnlineCoursesComponent} />
            <Stack.Screen name="ForumComponent" component={ForumComponent} />
            <Stack.Screen name="ClassroomCourseType" component={ClassroomCourseType} />
            <Stack.Screen name="FavouriteComponent" component={FavouriteComponent} />
            <Stack.Screen name="CourseDetails" component={CourseDetails} />
            <Stack.Screen name="CourseList" component={CourseList} />
            <Stack.Screen name="HomeComponents" component={HomeComponents} />
            <Stack.Screen name="ForgotPassword" component={ForgotPassword} />
            <Stack.Screen name="GetStartSignUp" component={GetStartSignUp} />
            <Stack.Screen name="SignUpComponent" component={SignUpComponent} />
            <Stack.Screen name="SplashScreen" component={SplashScreen} />
            <Stack.Screen name="LoginComponent" component={LoginComponent} />

        </Stack.Navigator>
    )
}
export default AuthScreen;