
import React, { Component } from 'react';
import {
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Image,
    Dimensions,
    TouchableOpacity,
    ImageBackground
} from 'react-native';
import HeaderScreen from "./../../Components/Header";
import Colors from "./../../Utils/Colors";
const screenHeight = Dimensions.get('window').height;
const screenWidth = Dimensions.get('window').width;
import fontComponent from "./../../Utils/fonts";
import fontFamilyComponent from "./../../Utils/fontFaimly";
import { TextInput } from 'react-native-gesture-handler';

const CareerIcon = require("./../../Images/Career/Career.png")
const BoxIcon = require("./../../Images/Career/Work.png")
const StudentIcon = require("./../../Images/Career/Student.png")
const interviewIcon = require("./../../Images/Career/interview.png")
const pdfIcon = require("./../../Images/Career/pdf.png")
const tickIcon = require("./../../Images/Career/tick.png")
import BottunComponent from "./../../Components/Button/index"

class CareerSupportComponent extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            flag: false
        }
    }


    handleListCell = () => {
        if (this.state.flag == false) {
            this.setState({ flag: true })
        }
    }

    render() {
        return (
            <View style={{ padding: 20, flex: 1 }}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <>
                        <Text style={{ color: "#2a2a2a", fontFamily: fontFamilyComponent.Muli_Bold, fontSize: fontComponent.large_size }}>Free Career Support</Text>

                        <View style={{ paddingTop: 20 }}>
                            <View style={{ flexDirection: "row", alignItems: "center" }}>
                                <Image source={BoxIcon} />
                                <Text style={{ paddingLeft: 10, color: "#1e85ce", fontFamily: fontFamilyComponent.Muli_SemiBold, fontSize: fontComponent.micro_size }}>Search for a new job</Text>
                            </View>

                            <View style={{ flexDirection: "row", alignItems: "center" }}>
                                <Image source={StudentIcon} />
                                <Text style={{ paddingLeft: 10, color: "#1e85ce", fontFamily: fontFamilyComponent.Muli_SemiBold, fontSize: fontComponent.micro_size }}>Explore career options in your field</Text>
                            </View>

                            <View style={{ flexDirection: "row", alignItems: "center" }}>
                                <Image source={tickIcon} />
                                <Text style={{ paddingLeft: 10, color: "#1e85ce", fontFamily: fontFamilyComponent.Muli_SemiBold, fontSize: fontComponent.micro_size }}>Build a stronger professional network and brand</Text>
                            </View>


                            <View style={{ flexDirection: "row", alignItems: "center" }}>
                                <Image source={pdfIcon} />
                                <Text style={{ paddingLeft: 10, color: "#1e85ce", fontFamily: fontFamilyComponent.Muli_SemiBold, fontSize: fontComponent.micro_size }}>Revise your resume or CV</Text>
                            </View>

                            <View style={{ flexDirection: "row", alignItems: "center" }}>
                                <Image source={interviewIcon} />
                                <Text style={{ paddingLeft: 10, color: "#1e85ce", fontFamily: fontFamilyComponent.Muli_SemiBold, fontSize: fontComponent.micro_size }}>Prepare for job interviews</Text>
                            </View>

                            <View style={{ flexDirection: "row", alignItems: "center" }}>
                                <Image source={CareerIcon} />
                                <Text style={{ paddingLeft: 10, color: "#1e85ce", fontFamily: fontFamilyComponent.Muli_SemiBold, fontSize: fontComponent.micro_size }}>Get your Dream Career</Text>
                            </View>


                            <View style={{ paddingVertical: 15 }}>
                                <Text style={{ color: "#2a2a2a", fontFamily: fontFamilyComponent.Muli_SemiBold, fontSize: fontComponent.medium_size }}>To request free career support service fill out the form below</Text>
                            </View>

                            <View style={{ paddingHorizontal: 20 }}>
                                <TextInput
                                    style={{ height: 40, borderBottomColor: Colors.grey, borderBottomWidth: 1, fontSize: fontComponent.small_size }}
                                    placeholder={"Name"}
                                    placeholderTextColor={"#6c6d72"}
                                    onChangeText={(val) => this.setState({ name: val })}
                                />
                                <View style={{ paddingVertical: 10 }}>
                                    <Text style={{ color: "#6c6d72", fontSize: fontComponent.small_size, fontFamily: fontFamilyComponent.Raleway_Regular }}>Language Spoken: </Text>
                                </View>

                                <View style={{ paddingVertical: 10 }}>
                                    <TextInput
                                        style={{ height: 40, borderBottomColor: Colors.grey, borderBottomWidth: 1, fontSize: fontComponent.small_size }}
                                        placeholder={"Any other Language"}
                                        placeholderTextColor={"#6c6d72"}
                                        onChangeText={(val) => this.setState({ name: val })}
                                    />
                                </View>

                                <View style={{ paddingVertical: 10 }}>
                                    <Text style={{ color: "#6c6d72", fontSize: fontComponent.small_size, fontFamily: fontFamilyComponent.Raleway_Regular }}>Language Spoken: </Text>
                                </View>

                                <View style={{ paddingVertical: 10 }}>
                                    <TextInput
                                        style={{ height: 40, borderBottomColor: Colors.grey, borderBottomWidth: 1, fontSize: fontComponent.small_size }}
                                        placeholder={"Date of Birth"}
                                        placeholderTextColor={"#6c6d72"}
                                        onChangeText={(val) => this.setState({ name: val })}
                                    />
                                </View>

                                <View style={{ paddingVertical: 10 }}>
                                    <TextInput
                                        style={{ height: 40, borderBottomColor: Colors.grey, borderBottomWidth: 1, fontSize: fontComponent.small_size }}
                                        placeholder={"Street Address"}
                                        placeholderTextColor={"#6c6d72"}
                                        onChangeText={(val) => this.setState({ name: val })}
                                    />
                                </View>

                                <View style={{ paddingVertical: 10 }}>
                                    <TextInput
                                        style={{ height: 40, borderBottomColor: Colors.grey, borderBottomWidth: 1, fontSize: fontComponent.small_size }}
                                        placeholder={"City"}
                                        placeholderTextColor={"#6c6d72"}
                                        onChangeText={(val) => this.setState({ name: val })}
                                    />
                                </View>

                                <View style={{ paddingVertical: 10 }}>
                                    <TextInput
                                        style={{ height: 40, borderBottomColor: Colors.grey, borderBottomWidth: 1, fontSize: fontComponent.small_size }}
                                        placeholder={"Post code"}
                                        placeholderTextColor={"#6c6d72"}
                                        onChangeText={(val) => this.setState({ name: val })}
                                    />
                                </View>


                                <View style={{ paddingVertical: 10 }}>
                                    <TextInput
                                        style={{ height: 40, borderBottomColor: Colors.grey, borderBottomWidth: 1, fontSize: fontComponent.small_size }}
                                        placeholder={"Country code"}
                                        placeholderTextColor={"#6c6d72"}
                                        onChangeText={(val) => this.setState({ name: val })}
                                    />
                                </View>


                                <View style={{ paddingVertical: 10 }}>
                                    <TextInput
                                        style={{ height: 40, borderBottomColor: Colors.grey, borderBottomWidth: 1, fontSize: fontComponent.small_size }}
                                        placeholder={"Phone Number"}
                                        placeholderTextColor={"#6c6d72"}
                                        onChangeText={(val) => this.setState({ name: val })}
                                    />
                                </View>


                                <View style={{ paddingTop: 20, alignSelf: "center" }}>
                                    <BottunComponent
                                        imageStyle={{ height: screenHeight / 10, width: screenWidth / 1.5 }}
                                        buttomTitle={"Send"}
                                    />
                                </View>

                            </View>




                        </View>


                    </>
                </ScrollView>
                
            </View>
        );
    }
};

const styles = StyleSheet.create({
    container: { backgroundColor: Colors.white, flex: 1 },
    imageLogo: { width: 120, height: 120, borderRadius: 60 },
    logoContainer: { flex: 0.5, justifyContent: "center", alignItems: "center" },
});

export default CareerSupportComponent;