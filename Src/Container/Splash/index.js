
import React, { Component } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Image,
  Dimensions,
  ImageBackground
} from 'react-native';
import Colors from "./../../Utils/Colors";
const screenHeight = Dimensions.get('window').height;
const screenWidth = Dimensions.get('window').width;
const appLog = require("./../../Images/Splash_logo.png");
import fontComponent from "./../../Utils/fonts"
import fontFamilyComponent from "./../../Utils/fontFaimly"
const SplashBG = require("./../../Images/SplashBG.png")
import * as Progress from 'react-native-progress';

class Login extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      progress: 0,
      indeterminate: true,
    };
  }

  componentDidMount() {

    setInterval(() => {
      this.props.navigation.navigate("LoginComponent")
    }, 3300);
    this.animate();
  }

  animate() {
    let progress = 0;
    this.setState({ progress });
    // setInterval(() => {
      this.setState({ indeterminate: false });
      setInterval(() => {
        progress += Math.random() / 5;
        // alert(progress)
        if (progress > 1) {
          progress = 1;
        }
        this.setState({ progress });
      }, 100);
    // }, 10);
  }


  render() {
    return (
      <>
        <StatusBar barStyle="dark-content" backgroundColor={Colors.statusBarBgColor} />
        <ImageBackground
          source={SplashBG}
          style={{ flex: 1 }}
          resizeMode={"cover"}
        >
          <View style={{ alignItems: "center", justifyContent: "center", flex: 1 }}>

            <View style={{ flex: 0.8, justifyContent: "center" }}>
              <Image
                source={appLog}
                style={styles.imageLogo}
              />
            </View>

            <View style={{ justifyContent: "center" }}>
              <Progress.Bar
                progress={this.state.progress}
                width={screenWidth / 1.43}
                color={"#99e5ff"}
                // animationConfig = { {bounciness: 0} }
                indeterminate={this.state.indeterminate}
              />
            </View>
          </View>
        </ImageBackground>
      </>
    );
  }
};

const styles = StyleSheet.create({
  imageLogo: { width: 150, height: 150, borderRadius: 75 }
});

export default Login;