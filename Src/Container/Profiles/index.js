
import React, { Component } from 'react';
import {
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Image,
    Dimensions,
    TouchableOpacity,
    ImageBackground,
    TextInput
} from 'react-native';
import HeaderScreen from "./../../Components/Header";
import Colors from "./../../Utils/Colors";
const screenHeight = Dimensions.get('window').height;
const screenWidth = Dimensions.get('window').width;
const appLog = require("./../../Images/logo.png");
const ButtonImage = require("./../../Images/Button.png");
import fontComponent from "./../../Utils/fonts";
import fontFamilyComponent from "./../../Utils/fontFaimly";
import TabViewComponent from "./../../Components/Comman/TabView";
import Footer from "./../../Components/Comman/Footer";
import ClassroomCoursesComponent from "./../Products/ClassroomCourses"
import FreebiesComponents from "./../Freebies"
import CareerSupport from "./../Career/index"
import BottunComponent from "./../../Components/Button/index"
const profilePic = require("./../../Images/Testing/image.png")
const PlusIcon = require("./../../Images/Plus.png")



class ProfilesComponent extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            tabBackgroundColor: Colors.tabColor,
            tabBackgroundColor1: Colors.headerColor,
            tabBackgroundColor2: Colors.headerColor,
            tabBackgroundColor3: Colors.headerColor,
            courseFlag: false,
            tabChange: "1"
        }
    }




    handleTabView = (val) => {
        this.setState({
            tabChange: val
        })
        if (val == "1") {
            this.setState({
                tabBackgroundColor: Colors.tabColor,
                tabBackgroundColor1: Colors.headerColor,
                tabBackgroundColor2: Colors.headerColor,
                tabBackgroundColor3: Colors.headerColo
            })
        } else if (val == "2") {
            this.setState({
                tabBackgroundColor: Colors.headerColor,
                tabBackgroundColor1: Colors.tabColor,
                tabBackgroundColor2: Colors.headerColor,
                tabBackgroundColor3: Colors.headerColo
            })
        } else if (val == "3") {
            this.setState({
                tabBackgroundColor: Colors.headerColor,
                tabBackgroundColor1: Colors.headerColor,
                tabBackgroundColor2: Colors.tabColor,
                tabBackgroundColor3: Colors.headerColo
            })
        } else if (val == "4") {
            this.setState({
                tabBackgroundColor: Colors.headerColor,
                tabBackgroundColor1: Colors.headerColor,
                tabBackgroundColor2: Colors.headerColor,
                tabBackgroundColor3: Colors.tabColor
            })
        }
    }


    handleProfiles() {

    }


    render() {
        return (
            <>
                <StatusBar barStyle="dark-content" backgroundColor={Colors.statusBarBgColor} />
                <>
                    <HeaderScreen
                        headerLeft={"Home"}
                    />
                    <View style={styles.container}>

                        <TabViewComponent
                            handleTabView={(val) => this.handleTabView(val)}
                            tabBackgroundColor={this.state.tabBackgroundColor}
                            tabBackgroundColor1={this.state.tabBackgroundColor1}
                            tabBackgroundColor2={this.state.tabBackgroundColor2}
                            tabBackgroundColor3={this.state.tabBackgroundColor3}
                        />

                        <View style={{ flex: 0.9, backgroundColor: Colors.white }}>
                            <ScrollView >
                                <View style={{ height: screenHeight / 1.2 }}>

                                    <View style={{ justifyContent: "center", alignItems: "center", height: screenHeight / 4.5 }}>
                                        <View style={{ width: 100, height: 100, borderRadius: 50, backgroundColor: "#efefef", alignItems: "center", justifyContent: "center" }}>
                                            <View style={{ width: 80, height: 80, borderRadius: 40, borderColor: Colors.headerColor, borderWidth: 4, alignItems: "center", justifyContent: "center" }}>

                                                <TouchableOpacity onPress={() => this.handleProfiles()}>
                                                    <Image source={profilePic} style={{ width: 60, position: "relative", height: 60, borderRadius: 30 }} />
                                                    <View style={{ alignItems: "center", justifyContent: "center", width: 30, height: 30, position: "absolute", bottom: -10, right: -10, borderRadius: 15, backgroundColor: Colors.black }}>
                                                        <Image source={PlusIcon} />
                                                    </View>
                                                </TouchableOpacity>

                                            </View>
                                        </View>
                                    </View>


                                    <View style={{ paddingHorizontal: 25 }}>


                                        <TextInput
                                            style={{ height: 40, borderBottomColor: Colors.grey, borderBottomWidth: 0.6, fontSize: fontComponent.small_size }}
                                            placeholder={"Name"}
                                            placeholderTextColor={"#6c6d72"}
                                            onChangeText={(val) => this.setState({ name: val })}
                                        />


                                        <View style={{ paddingTop: 20 }}>

                                            <TextInput
                                                style={{ height: 40, borderBottomColor: Colors.grey, borderBottomWidth: 0.6, fontSize: fontComponent.small_size }}
                                                placeholder={"Email"}
                                                placeholderTextColor={"#6c6d72"}
                                                onChangeText={(val) => this.setState({ name: val })}
                                            />

                                        </View>

                                        <View style={{ paddingTop: 20 }}>

                                            <TextInput
                                                style={{ height: 40, borderBottomColor: Colors.grey, borderBottomWidth: 0.6, fontSize: fontComponent.small_size }}
                                                placeholder={"Phone No"}
                                                placeholderTextColor={"#6c6d72"}
                                                onChangeText={(val) => this.setState({ name: val })}
                                            />

                                        </View>


                                        <View style={{ paddingTop: 20 }}>

                                            <TextInput
                                                style={{ height: 40, borderBottomColor: Colors.grey, borderBottomWidth: 0.6, fontSize: fontComponent.small_size }}
                                                placeholder={"Address"}
                                                placeholderTextColor={"#6c6d72"}
                                                onChangeText={(val) => this.setState({ name: val })}
                                            />

                                        </View>

                                        <View style={{ paddingTop: 20, paddingBottom: 20 }}>

                                            <TextInput
                                                style={{ height: 40, borderBottomColor: Colors.grey, borderBottomWidth: 0.6, fontSize: fontComponent.small_size }}
                                                placeholder={"DOB"}
                                                placeholderTextColor={"#6c6d72"}
                                                onChangeText={(val) => this.setState({ name: val })}
                                            />

                                        </View>


                                        <View style={{ paddingTop: 20, alignSelf: "center" }}>
                                            <TouchableOpacity
                                                activeOpacity={.6}
                                            >
                                                <ImageBackground source={ButtonImage}
                                                    style={{ width: screenWidth / 1.6, height: screenHeight / 11 }}
                                                >
                                                    <View style={{ alignItems: "center", justifyContent: "center", height: screenHeight / 11 }}>
                                                        <Text style={styles.buttomText}>Save</Text>
                                                    </View>
                                                </ImageBackground>
                                            </TouchableOpacity>
                                        </View>

                                    </View>


                                </View>
                            </ScrollView>
                        </View>

                        <Footer
                            handleHome={() => this.handleHome()}
                            handleCard={() => this.handleCard()}
                            handleSearch={() => this.handleSearch()}
                        />
                    </View>
                </>
            </>
        );
    }
};

const styles = StyleSheet.create({
    container: { backgroundColor: Colors.white, flex: 1 },
    imageLogo: { width: 120, height: 120, borderRadius: 60 },
    buttomText: { color: Colors.white, fontSize: fontComponent.large_size, fontFamily: fontFamilyComponent.Raleway_Medium },
    logoContainer: { flex: 0.5, justifyContent: "center", alignItems: "center" },
});

export default ProfilesComponent;