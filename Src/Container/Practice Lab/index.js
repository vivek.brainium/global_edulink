
import React, { Component } from 'react';
import {
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Image,
    Dimensions,
    TouchableOpacity,
    ImageBackground,
    FlatList
} from 'react-native';
import HeaderScreen from "./../../Components/Header";
import Colors from "./../../Utils/Colors";
const screenHeight = Dimensions.get('window').height;
const screenWidth = Dimensions.get('window').width;
import fontComponent from "./../../Utils/fonts";
import fontFamilyComponent from "./../../Utils/fontFaimly";
import TabViewComponent from "./../../Components/Comman/TabView";
import Footer from "./../../Components/Comman/Footer";
const ImageIcon = require("./../../Images/Testing/PracticeLab/image-1.png")
const ImageIcon1 = require("./../../Images/Testing/PracticeLab/image-2.png")


var data = [
    { "name": "Accounting & Finance", "images": ImageIcon },
    { "name": "Business $ Management", "images": ImageIcon1 },
    { "name": "Business $ Management", "images": ImageIcon },
    { "name": "Business $ Management", "images": ImageIcon1 },
    { "name": "Business $ Management", "images": ImageIcon },
    { "name": "Business $ Management", "images": ImageIcon1 },
]

class PracticeLabComponent extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            tabBackgroundColor: Colors.tabColor,
            tabBackgroundColor1: Colors.headerColor,
            tabBackgroundColor2: Colors.headerColor,
            tabBackgroundColor3: Colors.headerColor,
            courseFlag: false,
            tabChange: "1"
        }
    }




    handleTabView = (val) => {
        this.setState({
            tabChange: val
        })
        if (val == "1") {
            this.setState({
                tabBackgroundColor: Colors.tabColor,
                tabBackgroundColor1: Colors.headerColor,
                tabBackgroundColor2: Colors.headerColor,
                tabBackgroundColor3: Colors.headerColo
            })
        } else if (val == "2") {
            this.setState({
                tabBackgroundColor: Colors.headerColor,
                tabBackgroundColor1: Colors.tabColor,
                tabBackgroundColor2: Colors.headerColor,
                tabBackgroundColor3: Colors.headerColo
            })
        } else if (val == "3") {
            this.setState({
                tabBackgroundColor: Colors.headerColor,
                tabBackgroundColor1: Colors.headerColor,
                tabBackgroundColor2: Colors.tabColor,
                tabBackgroundColor3: Colors.headerColo
            })
        } else if (val == "4") {
            this.setState({
                tabBackgroundColor: Colors.headerColor,
                tabBackgroundColor1: Colors.headerColor,
                tabBackgroundColor2: Colors.headerColor,
                tabBackgroundColor3: Colors.tabColor
            })
        }
    }

    handleCard() {

    }
    handleSearch() {

    }
    handleHome() {

    }

    handleServices() {

    }

    handleCourses() {
        this.props.navigation.navigate("CourseList")
    }

    render() {
        return (
            <>
                <StatusBar barStyle="dark-content" backgroundColor={Colors.statusBarBgColor} />
                <>
                    <HeaderScreen
                        headerLeft={"Home"}
                    />
                    <View style={styles.container}>

                        <TabViewComponent
                            handleTabView={(val) => this.handleTabView(val)}
                            tabBackgroundColor={this.state.tabBackgroundColor}
                            tabBackgroundColor1={this.state.tabBackgroundColor1}
                            tabBackgroundColor2={this.state.tabBackgroundColor2}
                            tabBackgroundColor3={this.state.tabBackgroundColor3}
                        />


                        <View style={{ padding: 20, flex: 0.86 }}>
                            <Text style={{ color: "#2a2a2a", fontFamily: fontFamilyComponent.Muli_Bold, fontSize: fontComponent.large_size }}>Practice Lab</Text>

                            <View style={{ flexDirection: "row", justifyContent: "space-between", paddingTop: 20 }}>

                                <FlatList
                                    data={data}
                                    numColumns={2}
                                    renderItem={({ item }) =>
                                        <View style={{ width: screenWidth / 2.3, marginRight: 15 }}>
                                            <Image source={item.images} style={{ width: screenWidth / 2.3, height: screenHeight / 5 }} />

                                            <View style={{ backgroundColor: Colors.bottonColor, alignSelf: "center", borderRadius: 50, marginVertical: 20, width: screenWidth / 3.6 }}>
                                                <TouchableOpacity style={{ alignItems: "center", justifyContent: "center" }}>
                                                    <Text style={{ color: Colors.white, paddingVertical: 6, fontFamily: fontFamilyComponent.Raleway_Medium, fontSize: fontComponent.small_size }}>Learn More</Text>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                    }
                                    keyExtractor={item => item.id}
                                />
                            </View>
                        </View>


                        <Footer
                            handleHome={() => this.handleHome()}
                            handleCard={() => this.handleCard()}
                            handleSearch={() => this.handleSearch()}
                        />
                    </View>
                </>
            </>
        );
    }
};

const styles = StyleSheet.create({
    container: { backgroundColor: Colors.white, flex: 1 },
});

export default PracticeLabComponent;