
import React, { Component } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Image,
    Dimensions,
    TouchableOpacity,
    ImageBackground,
} from 'react-native';
import HeaderScreen from "../../Components/Header";
import Colors from "../../Utils/Colors";
const screenHeight = Dimensions.get('window').height;
const screenWidth = Dimensions.get('window').width;
import fontComponent from "../../Utils/fonts"
import fontFamilyComponent from "../../Utils/fontFaimly"
import CourseNameList from "./../../Components/Course/CourseCell"
import TabViewComponent from "./../../Components/Comman/TabView"
import Footer from "./../../Components/Comman/Footer"
const imageForCourse = require("./../../Images/Testing/image-01.png")
var subCourseTextArr = ["#017cb2", "#2a2a2a", "#2a2a2a", "#2a2a2a"];
const HeartIcon = require("./../../Images/Heart.png")
const HeartIcon1 = require("./../../Images/Heart_like.png")
const capIcon = require("./../../Images/Cap.png")
import Entypo from 'react-native-vector-icons/Entypo';
const ButtonImage = require("./../../Images/Button.png")


class CourseDetails extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            data: [1, 2, 3, 4, 5, 6, 7, 8],
            subCourseData: [
                { "id": 1, "name": "Overview" },
                { "id": 2, "name": "Curriculum" },
                { "id": 3, "name": "Review" },
                { "id": 4, "name": "Related Course" }
            ],
            subCourseText: ["#017cb2", "#2a2a2a", , "#2a2a2a", , "#2a2a2a"],
            likeFlag: false,
            rating: [1, 2, 3, 4, 5]
        }
    }

    componentDidMount() {

    }



    handleSubSourseType(item, index) {
        subCourseTextArr[index] = "#017cb2"

        // this.setState({
        //     subCourseText: subCourseTextArr
        // })
        // subCourseTextArr = "#2a2a2a"
    }


    handleLike() {
        if (this.state.likeFlag == false) {
            this.setState({
                likeFlag: true
            })
        } else {
            this.setState({
                likeFlag: false
            })
        }
    }


    render() {
        return (
            <>
                <StatusBar barStyle="dark-content" backgroundColor={Colors.statusBarBgColor} />
                <>
                    <HeaderScreen
                        headerLeft={"Course Details"}
                        onPress={() => this.props.navigation.navigate("CourseList")}
                    />
                    <View style={styles.container}>

                        <TabViewComponent
                            handleTabView={(val) => this.handleTabView(val)}
                            tabBackgroundColor={this.state.tabBackgroundColor}
                            tabBackgroundColor1={this.state.tabBackgroundColor1}
                            tabBackgroundColor2={this.state.tabBackgroundColor2}
                            tabBackgroundColor3={this.state.tabBackgroundColor3}
                        />
                        {/* <View> */}
                        <CourseNameList
                            data={this.state.data}
                        />
                        <View style={{ flex: 0.89 }}>
                            <ScrollView style={{}}>
                                <View style={{ flex: 1, marginBottom: 15 }}>

                                    {/* <View style={{ height: 10, backgroundColor: "#DDDDDD" }} /> */}

                                    <View style={{ flexDirection: "row", alignItems: "center", paddingHorizontal: 20, backgroundColor: "#DDD", padding: 10 }}>
                                        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                                            {this.state.subCourseData.map((item, index) => {
                                                return (
                                                    <TouchableOpacity style={{ paddingRight: 15 }} onPress={(item, index) => this.handleSubSourseType(item, index)}>
                                                        <Text style={{ color: this.state.subCourseText[index], fontFamily: fontFamilyComponent.Muli_Bold, fontSize: fontComponent.medium_size }}>{item.name}</Text>
                                                    </TouchableOpacity>
                                                )
                                            })}
                                        </ScrollView>
                                    </View>



                                    <View style={{ position: "relative" }}>
                                        <View style={{ margin: 20, backgroundColor: Colors.white }}>
                                            <Image source={imageForCourse} style={{ height: screenHeight / 4.2, width: screenWidth / 1.09 }} />
                                        </View>
                                        <View style={{ backgroundColor: Colors.white, position: "absolute", right: 30, bottom: 10, alignItems: "center", justifyContent: "center", height: 40, width: 40, borderRadius: 20, elevation: 3 }}>
                                            {
                                                this.state.likeFlag == false ?
                                                    <TouchableOpacity onPress={() => this.handleLike()}>
                                                        <Image source={HeartIcon} style={{ width: 23, height: 20 }} />
                                                    </TouchableOpacity>
                                                    :
                                                    <TouchableOpacity onPress={() => this.handleLike()}>
                                                        <Image source={HeartIcon1} style={{ width: 23, height: 20 }} />
                                                    </TouchableOpacity>
                                            }
                                        </View>
                                    </View>


                                    <View style={{ paddingHorizontal: 20 }}>

                                        <Text style={{ color: "#040f14", fontSize: fontComponent.small_size, fontFamily: fontFamilyComponent.Raleway_SemiBold }}>International General Certificate in Occupational Health and Safety - Official Exam Included</Text>
                                        <View style={{ flexDirection: "row", alignItems: "flex-end", paddingTop: 5 }}>
                                            <Text style={{ color: "#a6a6a6", textAlign: "justify", fontFamily: fontFamilyComponent.Muli_SemiBold }}>$159</Text>
                                            <Text style={{ color: "#125e9a", fontSize: fontComponent.medium_size, fontFamily: fontFamilyComponent.Muli_Bold, paddingHorizontal: 2 }}>$59</Text>
                                            <Text style={{ fontFamily: fontFamilyComponent.Muli_SemiBold }}>(<Text style={{ color: "#051c2e", fontFamily: fontFamilyComponent.Muli_SemiBold }}>inc</Text><Text style={{ fontFamily: fontFamilyComponent.Muli_SemiBold }}>. VAT)</Text></Text>
                                        </View>
                                    </View>


                                    <View style={{ marginHorizontal: 20, paddingTop: 15 }}>
                                        <View style={{ height: 1, backgroundColor: "#DDD" }} />
                                        <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                                            <View style={{ flexDirection: "row", paddingVertical: 10, }}>
                                                <Image source={capIcon} style={{ width: 25, height: 25 }} />
                                                <Text style={{ paddingLeft: 5, textAlign: "center", color: "#00b9d4", fontSize: fontComponent.small_size, fontFamily: fontFamilyComponent.Raleway_Medium }}>254 Students</Text>
                                            </View>

                                            <View style={{ flexDirection: 'row', alignItems: "center", }}>

                                                <View style={{ flexDirection: "row" }}>
                                                    {this.state.rating.map(() => {
                                                        return (
                                                            <Entypo
                                                                name="star"
                                                                size={20}
                                                                color={"#eeb327"}
                                                            />
                                                        )
                                                    })}
                                                </View>

                                                <View>
                                                    <Text style={{ color: "#686e71", paddingLeft: 5, fontSize: fontComponent.small_size, fontFamily: fontFamilyComponent.Muli_SemiBold }}>(3 REVIEWS)</Text>
                                                </View>

                                            </View>
                                        </View>

                                        <View style={{ height: 1, backgroundColor: "#DDD" }} />
                                    </View>


                                    <View style={{ paddingHorizontal: 25 }}>
                                        <Text style={{ fontFamily: fontFamilyComponent.Muli_SemiBold, fontSize: fontComponent.medium_size, color: "#0d0d0e" }}>What Will I Learn?</Text>

                                        <View style={{ paddingTop: 15 }}>

                                            <View style={{ flexDirection: "row" }}>
                                                <View style={{ paddingTop: 10 }}>
                                                    <View style={{ borderRadius: 5, width: 10, height: 10, borderColor: "#000", borderWidth: 1 }} />
                                                </View>
                                                <Text style={{ fontSize: fontComponent.small_size, paddingLeft: 10, fontFamily: fontFamilyComponent.Muli_SemiBold, color: "#4b5054" }}>You will learn to manage health and safety effectively in the workplace</Text>
                                            </View>


                                            <View style={{ flexDirection: "row" }}>
                                                <View style={{ paddingTop: 10 }}>
                                                    <View style={{ borderRadius: 5, width: 10, height: 10, borderColor: "#000", borderWidth: 1 }} />
                                                </View>
                                                <Text style={{ fontSize: fontComponent.small_size, paddingLeft: 10, fontFamily: fontFamilyComponent.Muli_SemiBold, color: "#4b5054" }}>You will learn to manage health and safety effectively in the workplace</Text>
                                            </View>


                                            <View style={{ flexDirection: "row" }}>
                                                <View style={{ paddingTop: 10 }}>
                                                    <View style={{ borderRadius: 5, width: 10, height: 10, borderColor: "#000", borderWidth: 1 }} />
                                                </View>
                                                <Text style={{ fontSize: fontComponent.small_size, paddingLeft: 10, fontFamily: fontFamilyComponent.Muli_SemiBold, color: "#4b5054" }}>You will learn to manage health and safety effectively in the workplace</Text>
                                            </View>


                                            <View style={{ flexDirection: "row" }}>
                                                <View style={{ paddingTop: 10 }}>
                                                    <View style={{ borderRadius: 5, width: 10, height: 10, borderColor: "#000", borderWidth: 1 }} />
                                                </View>
                                                <Text style={{ fontSize: fontComponent.small_size, paddingLeft: 10, fontFamily: fontFamilyComponent.Muli_SemiBold, color: "#4b5054" }}>You will learn to manage health and safety effectively in the workplace</Text>
                                            </View>


                                        </View>

                                        <Text style={{ color: "#0d0d0e", fontFamily: fontFamilyComponent.Muli_SemiBold, fontSize: fontComponent.medium_size, paddingVertical: 20 }}>Overview</Text>
                                        <Text style={{ fontFamily: fontFamilyComponent.Muli_SemiBold, color: "#0d0d0e", fontSize: fontComponent.small_size }}>Health and safety is the gold standard, so it is no surprise that over 200,000 people alone in the UK hold this National General Certificate in Occupational Health and safety.</Text>


                                        <View style={{ flexDirection: "row", paddingVertical: 35, justifyContent: "space-between" }}>
                                            <View style={{ backgroundColor: Colors.headerColor, width: screenWidth / 2.5, justifyContent: "center", alignItems: "center", borderRadius: 50 }}>
                                                <TouchableOpacity>
                                                    <Text style={{ color: Colors.white, paddingVertical: 12, fontFamily: fontFamilyComponent.Raleway_Medium, fontSize: fontComponent.small_size }}>Add to cart</Text>
                                                </TouchableOpacity>
                                            </View>

                                            <View style={{ backgroundColor: "#eeb327", width: screenWidth / 2.5, justifyContent: "center", alignItems: "center", borderRadius: 50 }}>
                                                <TouchableOpacity>
                                                    <Text style={{ color: Colors.white, paddingVertical: 12, fontFamily: fontFamilyComponent.Raleway_Medium, fontSize: fontComponent.small_size }}>Buy now</Text>
                                                </TouchableOpacity>
                                            </View>

                                        </View>

                                    </View>
                                </View>
                            </ScrollView>
                        </View>

                        <Footer
                            handleHome={() => this.handleHome()}
                            handleCard={() => this.handleCard()}
                            handleSearch={() => this.handleSearch()}
                            flagForMessage={true}
                        />
                    </View>
                </>
            </>
        );
    }
};

const styles = StyleSheet.create({
    container: { backgroundColor: Colors.white, flex: 1 },
    buttomImage: { height: screenHeight / 14.85, width: screenWidth / 2.4, justifyContent: "center" },
    buttomStyle: { justifyContent: "center", alignItems: "center", height: screenHeight / 11.5 },
    buttomText: { color: Colors.white, fontSize: fontComponent.small_size, fontFamily: fontFamilyComponent.Raleway_Medium },
});

export default CourseDetails;