
import React, { Component } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Image,
    Dimensions,
    TouchableOpacity,
    FlatList
} from 'react-native';
import HeaderScreen from "./../../Components/Header";
import Colors from "./../../Utils/Colors";
const screenHeight = Dimensions.get('window').height;
const screenWidth = Dimensions.get('window').width;
import fontComponent from "./../../Utils/fonts"
import fontFamilyComponent from "./../../Utils/fontFaimly"
const Images1 = require("./../../Images/Testing/image-1.png")
const HeartIcon = require("./../../Images/Heart.png")
const HeartIcon1 = require("./../../Images/Heart_like.png")
const ListcheckIcon = require("./../../Images/list-check.png")
const ListUncheckIcon = require("./../../Images/list-uncheck.png")
import Entypo from 'react-native-vector-icons/Entypo';
import CourseListCell from "./../../Components/Course/CourseListCell"
import CourseNameList from "./../../Components/Course/CourseCell"
import TabViewComponent from "./../../Components/Comman/TabView"
import Footer from "./../../Components/Comman/Footer"

class CourseListComponent extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            data: [1, 2, 3, 4, 5, 6, 7, 8],
            heartFlag: false,
            listFlag: false,
            rating: [1, 2, 3, 4, 5],
            tabBackgroundColor: Colors.tabColor,
            tabBackgroundColor1: Colors.headerColor,
            tabBackgroundColor2: Colors.headerColor,
            tabBackgroundColor3: Colors.headerColor,
        }
    }

    handleTabView = (val) => {
        if (val == "1") {
            this.setState({
                tabBackgroundColor: Colors.tabColor,
                tabBackgroundColor1: Colors.headerColor,
                tabBackgroundColor2: Colors.headerColor,
                tabBackgroundColor3: Colors.headerColo
            })
        } else if (val == "2") {
            this.setState({
                tabBackgroundColor: Colors.headerColor,
                tabBackgroundColor1: Colors.tabColor,
                tabBackgroundColor2: Colors.headerColor,
                tabBackgroundColor3: Colors.headerColo
            })
        } else if (val == "3") {
            this.setState({
                tabBackgroundColor: Colors.headerColor,
                tabBackgroundColor1: Colors.headerColor,
                tabBackgroundColor2: Colors.tabColor,
                tabBackgroundColor3: Colors.headerColo
            })
        } else if (val == "4") {
            this.setState({
                tabBackgroundColor: Colors.headerColor,
                tabBackgroundColor1: Colors.headerColor,
                tabBackgroundColor2: Colors.headerColor,
                tabBackgroundColor3: Colors.tabColor
            })
        }
    }

    handleChangeHeart() {
        if (this.state.heartFlag) {
            this.setState({
                heartFlag: false
            })
        } else {
            this.setState({
                heartFlag: true
            })
        }
    }

    handleOnchangeList() {
        if (this.state.listFlag) {
            this.setState({
                listFlag: false
            })
        } else {
            this.setState({
                listFlag: true
            })
        }
    }

    handleCourseList() {
        this.props.navigation.navigate("CourseDetails")
    }



    render() {
        return (
            <>
                <StatusBar barStyle="dark-content" backgroundColor={Colors.statusBarBgColor} />
                <>
                    <HeaderScreen
                        headerLeft={"Home"}
                    />
                    <View style={styles.container}>
                        <View style={{ flex: 0.6 }}>
                            <TabViewComponent
                                handleTabView={(val) => this.handleTabView(val)}
                                tabBackgroundColor={this.state.tabBackgroundColor}
                                tabBackgroundColor1={this.state.tabBackgroundColor1}
                                tabBackgroundColor2={this.state.tabBackgroundColor2}
                                tabBackgroundColor3={this.state.tabBackgroundColor3}
                            />

                            <CourseNameList
                                data={this.state.data}
                            />
                            <View style={{ height: 10, backgroundColor: "#DDDDDD" }} />


                            <View>
                            </View>

                            <Text style={{ paddingHorizontal: 25, paddingVertical: 5, fontFamily: fontFamilyComponent.Muli_Bold, fontSize: fontComponent.large_size, color: "#2a2a2a" }}>Popular Sub Cource</Text>

                            <View style={{ paddingHorizontal: 25 }}>

                                <FlatList
                                    data={this.state.data}
                                    renderItem={({ item }) => <CourseListCell handleCourseList={() => this.handleCourseList()} />}
                                    keyExtractor={item => item.id}
                                />


                            </View>
                        </View>
                        <Footer
                            handleHome={() => this.handleHome()}
                            handleCard={() => this.handleCard()}
                            handleSearch={() => this.handleSearch()}
                        />
                    </View>


                </>
            </>
        );
    }
};

const styles = StyleSheet.create({
    container: { backgroundColor: Colors.white, flex: 1 },
});

export default CourseListComponent;