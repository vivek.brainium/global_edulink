
import React, {  useState } from 'react';
import {
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Image,
    Dimensions,
    TouchableOpacity,
    ImageBackground,
    FlatList
} from 'react-native';
import HeaderScreen from "./../../Components/Header";
import Colors from "./../../Utils/Colors";
const screenHeight = Dimensions.get('window').height;
const screenWidth = Dimensions.get('window').width;
import fontComponent from "./../../Utils/fonts";
import fontFamilyComponent from "./../../Utils/fontFaimly";
const ArrowIcon = require("./../../Images/Arrow.png")
import FreebiesCell from "./../../Components/Freebies/FreebiesCell"
const FreebiesIcon = require("./../../Images/Testing/freebis.png")
const FreebiesIcon01 = require("./../../Images/Testing/freebis01.png")
const FreebiesIcon02 = require("./../../Images/Testing/freebis02.png")
const FreebiesIcon03 = require("./../../Images/Testing/freebis03.png")


const FreebiesComponents = () => {
    const [data, setData] = useState([1,2,3,4,5,6])
    return (
        <>
            <View style={styles.container}>
                <View style={{ padding: 25, alignItems: "center" }}>
                    <TouchableOpacity activeOpacity={.5} style={{
                        flexDirection: "row", width: screenWidth / 1.4, shadowOffset: { width: 0, height: 2 }, borderTopWidth: 0,
                        shadowColor: 'black',
                        shadowOpacity: 1,
                        elevation: 0.5,
                        zIndex: 999, paddingVertical: 15, justifyContent: "center", alignItems: "center"
                    }}>
                        <Text style={{ paddingRight: 10, color: "#1e85ce", fontFamily: fontFamilyComponent.Muli_Bold, fontSize: fontComponent.small_size }}>Easy steps to claim free courses</Text>
                        <Image source={ArrowIcon} />
                    </TouchableOpacity>
                </View>

                <View style={{ paddingHorizontal: 20 }}>
                    <Text style={{ color: "#2a2a2a", fontFamily: fontFamilyComponent.Muli_Bold, fontSize: fontComponent.medium_size }}>Free Ccourses</Text>
                    <View style={{ flexDirection: "row", justifyContent: "space-between" }}>

                        <FlatList
                            data={data}
                            numColumns={2}
                            renderItem={({ item }) => <FreebiesCell />}
                            keyExtractor={item => item.id}
                        />

                    </View>
                </View>
            </View>
        </>
    )
};

const styles = StyleSheet.create({
    container: { backgroundColor: Colors.white, flex: 0.75 }
});

export default FreebiesComponents;