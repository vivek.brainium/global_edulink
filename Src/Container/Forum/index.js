import React, { useState } from 'react';
import {
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Image,
    Dimensions,
    TouchableOpacity,
    ImageBackground
} from 'react-native';
import Colors from "../../Utils/Colors";
const screenHeight = Dimensions.get('window').height;
const screenWidth = Dimensions.get('window').width;
import fontComponent from "../../Utils/fonts";
import fontFamilyComponent from "../../Utils/fontFaimly";
import Footer from "./../../Components/Comman/Footer";
import HeaderComponent from "./../../Components/Header";
const userIcon = require("./../../Images/Testing/user01.png")
const userIcon01 = require("./../../Images/Testing/user02.png")
const userIcon02 = require("./../../Images/Testing/user03.png")


const ForumComponent = () => {
    const [categoryFlag, setCategoryFlag] = useState(false)

    const handleCreatePost = () => {
        setCategoryFlag(false)
    }

    const handleCategory = () => {
        setCategoryFlag(true)
    }

    return (
        <>
            <StatusBar barStyle="dark-content" backgroundColor={Colors.statusBarBgColor} />
            <>
                <HeaderComponent
                    headerLeft={"Forum"}
                />
                <View style={{ paddingBottom: 20, flex: 0.97, backgroundColor: Colors.white }}>
                    <ScrollView showsVerticalScrollIndicator={false} style={{ flex: 1 }}>

                        <View style={{ backgroundColor: Colors.white, padding: 20 }}>

                            <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                                <TouchableOpacity onPress={() => handleCreatePost()} activeOpacity={0.6} style={{ backgroundColor: categoryFlag == false ? "#eeb427" : Colors.white, borderRadius: 50 }}>
                                    <Text style={{ color: categoryFlag == true ? "#017cb2" : Colors.white, paddingHorizontal: 25, paddingVertical: 10, fontSize: fontComponent.small_size, fontFamily: fontFamilyComponent.Raleway_Medium }}>Create a new post</Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => handleCategory()} activeOpacity={0.6} style={{ backgroundColor: categoryFlag == true ? "#eeb427" : Colors.white, borderRadius: 50, width: screenWidth / 2.3, alignItems: "center" }}>
                                    <Text style={{ color: categoryFlag == false ? "#017cb2" : Colors.white, paddingVertical: 10, fontSize: fontComponent.small_size, fontFamily: fontFamilyComponent.Raleway_Medium }}>Category</Text>
                                </TouchableOpacity>
                            </View>

                            <View style={{ paddingTop: 20 }}>
                                <Text style={{ color: "#2a2a2a", fontFamily: fontFamilyComponent.Muli_Bold, fontSize: fontComponent.large_size }}>Announcements</Text>
                            </View>

                            <View style={{ paddingVertical: 20, flexDirection: "row", alignItems: "center" }}>
                                <Image source={userIcon02} style={{ width: 50, height: 50, borderRadius: 25 }} />
                                <Text style={{ color: "#b2b2b2", paddingLeft: 10, fontFamily: fontFamilyComponent.Muli_SemiBold, fontSize: fontComponent.small_size }}>Lorem ipsum dolor sit amet, consectetur</Text>
                            </View>


                            <View style={{ paddingBottom: 20 }}>
                                <Text style={{ color: "#2a2a2a", fontSize: fontComponent.large_size, fontFamily: fontFamilyComponent.Muli_Bold }}>Support</Text>
                            </View>

                            <View style={{ paddingVertical: 10, flexDirection: "row", alignItems: "center" }}>
                                <Image source={userIcon} style={{ width: 50, height: 50, borderRadius: 25 }} />
                                <Text style={{ color: "#b2b2b2", paddingLeft: 10, fontFamily: fontFamilyComponent.Muli_SemiBold, fontSize: fontComponent.small_size }}>Lorem ipsum dolor sit amet, consectetur</Text>
                            </View>


                            <View style={{ paddingVertical: 10, flexDirection: "row", alignItems: "center" }}>
                                <Image source={userIcon01} style={{ width: 50, height: 50, borderRadius: 25 }} />
                                <Text style={{ color: "#b2b2b2", paddingLeft: 10, fontFamily: fontFamilyComponent.Muli_SemiBold, fontSize: fontComponent.small_size }}>Lorem ipsum dolor sit amet, consectetur</Text>
                            </View>

                            <View style={{ paddingVertical: 10, flexDirection: "row", alignItems: "center" }}>
                                <Image source={userIcon} style={{ width: 50, height: 50, borderRadius: 25 }} />
                                <Text style={{ color: "#b2b2b2", paddingLeft: 10, fontFamily: fontFamilyComponent.Muli_SemiBold, fontSize: fontComponent.small_size }}>Lorem ipsum dolor sit amet, consectetur</Text>
                            </View>


                            <View style={{ paddingVertical: 10, flexDirection: "row", alignItems: "center" }}>
                                <Image source={userIcon01} style={{ width: 50, height: 50, borderRadius: 25 }} />
                                <Text style={{ color: "#b2b2b2", paddingLeft: 10, fontFamily: fontFamilyComponent.Muli_SemiBold, fontSize: fontComponent.small_size }}>Lorem ipsum dolor sit amet, consectetur</Text>
                            </View>
                            <View style={{ paddingVertical: 10, flexDirection: "row", alignItems: "center" }}>
                                <Image source={userIcon} style={{ width: 50, height: 50, borderRadius: 25 }} />
                                <Text style={{ color: "#b2b2b2", paddingLeft: 10, fontFamily: fontFamilyComponent.Muli_SemiBold, fontSize: fontComponent.small_size }}>Lorem ipsum dolor sit amet, consectetur</Text>
                            </View>


                            <View style={{ paddingVertical: 10, flexDirection: "row", alignItems: "center" }}>
                                <Image source={userIcon01} style={{ width: 50, height: 50, borderRadius: 25 }} />
                                <Text style={{ color: "#b2b2b2", paddingLeft: 10, fontFamily: fontFamilyComponent.Muli_SemiBold, fontSize: fontComponent.small_size }}>Lorem ipsum dolor sit amet, consectetur</Text>
                            </View>

                        </View>

                    </ScrollView>
                </View>

                <Footer
                    handleHome={() => this.handleHome()}
                    handleCard={() => this.handleCard()}
                    handleSearch={() => this.handleSearch()}
                />
            </>
        </>
    )
}

export default ForumComponent;