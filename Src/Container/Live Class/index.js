
import React, { Component } from 'react';
import {
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Image,
    Dimensions,
    TouchableOpacity,
    ImageBackground,
    TextInput,
    FlatList
} from 'react-native';
import HeaderScreen from "./../../Components/Header";
import Colors from "./../../Utils/Colors";
const screenHeight = Dimensions.get('window').height;
const screenWidth = Dimensions.get('window').width;
import fontComponent from "./../../Utils/fonts";
import fontFamilyComponent from "./../../Utils/fontFaimly";
import TabViewComponent from "./../../Components/Comman/TabView";
import Footer from "./../../Components/Comman/Footer";
import BottunComponent from "./../../Components/Button/index"
const OnlineClass1 = require("./../../Images/Online_class/image-1.png")
const OnlineClass2 = require("./../../Images/Online_class/image-b2.png")


class LiveOnlineClassComponent extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            tabBackgroundColor: Colors.tabColor,
            tabBackgroundColor1: Colors.headerColor,
            tabBackgroundColor2: Colors.headerColor,
            tabBackgroundColor3: Colors.headerColor,
            courseFlag: false,
            tabChange: "1",
            data: [1,2,3, 4,5]
        }
    }




    handleTabView = (val) => {
        this.setState({
            tabChange: val
        })
        if (val == "1") {
            this.setState({
                tabBackgroundColor: Colors.tabColor,
                tabBackgroundColor1: Colors.headerColor,
                tabBackgroundColor2: Colors.headerColor,
                tabBackgroundColor3: Colors.headerColo
            })
        } else if (val == "2") {
            this.setState({
                tabBackgroundColor: Colors.headerColor,
                tabBackgroundColor1: Colors.tabColor,
                tabBackgroundColor2: Colors.headerColor,
                tabBackgroundColor3: Colors.headerColo
            })
        } else if (val == "3") {
            this.setState({
                tabBackgroundColor: Colors.headerColor,
                tabBackgroundColor1: Colors.headerColor,
                tabBackgroundColor2: Colors.tabColor,
                tabBackgroundColor3: Colors.headerColo
            })
        } else if (val == "4") {
            this.setState({
                tabBackgroundColor: Colors.headerColor,
                tabBackgroundColor1: Colors.headerColor,
                tabBackgroundColor2: Colors.headerColor,
                tabBackgroundColor3: Colors.tabColor
            })
        }
    }


    handleProfiles() {

    }


    render() {
        return (
            <>
                <StatusBar barStyle="dark-content" backgroundColor={Colors.statusBarBgColor} />
                <>
                    <HeaderScreen
                        headerLeft={"Home"}
                    />
                    <View style={styles.container}>

                        <TabViewComponent
                            handleTabView={(val) => this.handleTabView(val)}
                            tabBackgroundColor={this.state.tabBackgroundColor}
                            tabBackgroundColor1={this.state.tabBackgroundColor1}
                            tabBackgroundColor2={this.state.tabBackgroundColor2}
                            tabBackgroundColor3={this.state.tabBackgroundColor3}
                        />

                        <View style={{ flex: 0.9, backgroundColor: Colors.white }}>

                            <View style={{ padding: 25 }}>
                                <Text style={{ color: "#2a2a2a", fontFamily: fontFamilyComponent.Muli_Bold, fontSize: fontComponent.large_size }}>Live Online classes</Text>
                            </View>

                            {/* <ScrollView > */}
                            <View style={{ height: screenHeight / 1.7, paddingHorizontal: 25 }}>

                                <FlatList
                                    data={this.state.data}
                                    // numColumns={2}
                                    renderItem={({ item }) =>
                                        <View style={{ marginBottom: 15 }}>
                                            <Image source={OnlineClass1} style={{ position: "relative", width: screenWidth / 1.13, height: screenHeight / 3.5 }} />

                                            <View style={{ position: 'absolute', bottom: 0 }}>
                                                <Text style={{ color: Colors.white, paddingHorizontal: 15, fontSize: fontComponent.medium_size, fontFamily: fontFamilyComponent.Raleway_SemiBold }}>Foundation certificate in business analysis</Text>
                                                <View style={{ backgroundColor: 'rgba(52, 52, 52, 0.3)' }}>
                                                    <Text style={{ color: Colors.white, paddingHorizontal: 15, paddingVertical: 5, fontSize: 12, fontFamily: fontFamilyComponent.Raleway_Regular }}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra.</Text>
                                                </View>
                                            </View>
                                        </View>
                                    }
                                    keyExtractor={item => item.id}
                                />





                            </View>
                            {/* </ScrollView> */}
                        </View>

                        <Footer
                            handleHome={() => this.handleHome()}
                            handleCard={() => this.handleCard()}
                            handleSearch={() => this.handleSearch()}
                        />
                    </View>
                </>
            </>
        );
    }
};

const styles = StyleSheet.create({
    container: { backgroundColor: Colors.white, flex: 1 },
    imageLogo: { width: 120, height: 120, borderRadius: 60 },
    buttomText: { color: Colors.white, fontSize: fontComponent.large_size, fontFamily: fontFamilyComponent.Raleway_Medium },
    logoContainer: { flex: 0.5, justifyContent: "center", alignItems: "center" },
});

export default LiveOnlineClassComponent;