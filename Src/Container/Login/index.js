
import React, { Component } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Image,
    Dimensions,
    ImageBackground
} from 'react-native';
import HeaderScreen from "./../../Components/Header";
import Colors from "./../../Utils/Colors";
import TextField from "./../../Components/Comman/TextComponent"
import Fontisto from 'react-native-vector-icons/Fontisto';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import AntDesign from 'react-native-vector-icons/AntDesign';
const TestImage = require("./../../Images/Testing/logo.png");
const screenHeight = Dimensions.get('window').height;
const screenWidth = Dimensions.get('window').width;
import Ionicons from 'react-native-vector-icons/Ionicons';
import { TouchableOpacity } from 'react-native-gesture-handler';
const appLog = require("./../../Images/logo.png");
const ButtonImage = require("./../../Images/Button.png")
import fontComponent from "./../../Utils/fonts"
import fontFamilyComponent from "./../../Utils/fontFaimly"


class Login extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            passwordFlag: true,
            password: ""
        }
    }

    handleChangeIcon = () => {
        if (this.state.passwordFlag) {
            this.setState({
                passwordFlag: false
            })
        } else {
            this.setState({
                passwordFlag: true
            })
        }
    }

    handleSignUp = () => {
        this.props.navigation.navigate("SignUpComponent")
    }

    render() {
        return (
            <>
                <StatusBar barStyle="dark-content" backgroundColor={Colors.statusBarBgColor} />
                <>
                    <HeaderScreen
                        headerLeft={"Sign in"}
                    />
                    <View style={styles.container}>
                        <ScrollView
                            contentInsetAdjustmentBehavior="automatic"
                            showsVerticalScrollIndicator={false}
                        >
                            <View style={styles.sub_container}>

                                <View style={styles.image}>
                                    <Image
                                        source={appLog}
                                        style={styles.imageLogo}
                                    />
                                </View>

                                <View style={styles.main_input}>
                                    <View style={{ flex: 0.5 }}>

                                        <TextField
                                            // onChangeText={()=> this}
                                            secureTextEntry={false}
                                            icon={true}
                                            iconName={FontAwesome}
                                            iconType={"mobile"}
                                            iconColor={Colors.headerColor}
                                            size={40}
                                            iconStyle={{ paddingHorizontal: 5 }}
                                        />
                                    </View>

                                    <View style={{ paddingTop: 20, flex: 0.5 }}>
                                        <TextField
                                            onChangeText={(value) => this.setState({ password: value })}
                                            secureTextEntry={this.state.passwordFlag ? true : false}
                                            icon={true}
                                            iconName={AntDesign}
                                            iconType={"lock1"}
                                            iconColor={Colors.headerColor}
                                            size={28}
                                            iconRight={true}
                                            iconNameRight={Ionicons}
                                            iconTypeRight={this.state.passwordFlag ? "md-eye" : "md-eye-off"}
                                            iconColorRight={Colors.black}
                                            iconStyleRight={{ paddingTop: 5 }}
                                            sizeRight={30}
                                            onPressRight={() => this.handleChangeIcon()}
                                        />
                                    </View>
                                </View>

                                <View style={styles.forgot}>
                                    <TouchableOpacity>
                                        <Text style={styles.forgot_text}>Forgot</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity>
                                        <Text style={styles.forgot_text}>{" / Change password ?"}</Text>
                                    </TouchableOpacity>
                                </View>


                                <View style={styles.buttom}>
                                    <TouchableOpacity
                                        activeOpacity={.6}
                                    >
                                        <ImageBackground source={ButtonImage}
                                            style={styles.buttomImage}
                                        >
                                            <View style={styles.buttomStyle}>
                                                <Text style={styles.buttomText}>Sign in</Text>
                                            </View>
                                        </ImageBackground>
                                    </TouchableOpacity>
                                </View>

                                <View style={styles.dontHave}>
                                    <Text style={styles.dontText}>Don't have an account</Text>

                                    <TouchableOpacity onPress={()=> this.handleSignUp()}>
                                        <Text style={styles.signUpText}>Sign up for Free</Text>
                                    </TouchableOpacity>
                                </View>


                            </View>
                        </ScrollView>
                    </View>
                </>
            </>
        );
    }
};

const styles = StyleSheet.create({
    container: { paddingHorizontal: 25, backgroundColor: Colors.white, flex: 1 },
    sub_container: { height: screenHeight, backgroundColor: Colors.white },
    imageLogo: { width: 120, height: 120, borderRadius: 60 },
    image: { alignItems: "center", justifyContent: "center", flex: 0.35 },
    main_input: { flex: 0.24 },
    forgot: { paddingTop: 30, flexDirection: "row", justifyContent: "center", alignItems: "center" },
    forgot_text: { color: Colors.forgotTextColor, fontFamily: fontFamilyComponent.Raleway_Medium, fontSize: fontComponent.small_size },
    buttom: { justifyContent: "center", alignItems: "center", flex: 0.2 },
    buttomImage: { height: screenHeight / 12, width: screenWidth / 1.9 },
    buttomStyle: { justifyContent: "center", alignItems: "center", height: screenHeight / 11.5 },
    buttomText: { color: Colors.white, fontSize: fontComponent.large_size, fontFamily: fontFamilyComponent.Raleway_Medium },
    dontHave: { justifyContent: "center", alignItems: "center" },
    dontText: { color: "#464b4d", fontSize: fontComponent.small_size, fontFamily: fontFamilyComponent.Raleway_Medium },
    signUpText: { color: "#097bb6", fontSize: fontComponent.medium_size, fontFamily: fontFamilyComponent.Raleway_Medium, paddingTop: 10 },

});

export default Login;