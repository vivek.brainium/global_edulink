
import React, { Component } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Image,
    Dimensions,
    TouchableOpacity,
    ImageBackground,
    FlatList
} from 'react-native';
import HeaderScreen from "../../Components/Header";
import Colors from "../../Utils/Colors";
const screenHeight = Dimensions.get('window').height;
const screenWidth = Dimensions.get('window').width;
import fontComponent from "../../Utils/fonts"
import fontFamilyComponent from "../../Utils/fontFaimly"
import CourseNameList from "./../../Components/Course/CourseCell"
import TabViewComponent from "./../../Components/Comman/TabView"
import Footer from "./../../Components/Comman/Footer"
const imageForCourse = require("./../../Images/Testing/image-01.png")
var subCourseTextArr = ["#017cb2", "#2a2a2a", "#2a2a2a", "#2a2a2a"];
const HeartIcon = require("./../../Images/Heart.png")
const HeartIcon1 = require("./../../Images/Heart_like.png")
const capIcon = require("./../../Images/Cap.png")
import Entypo from 'react-native-vector-icons/Entypo';
const ButtonImage = require("./../../Images/Button.png")
import CourseListCell from "./../../Components/Course/CourseListCell"

class FavouriteComponent extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            data: [1, 2, 3, 4, 5, 6, 7, 8],
            // subCourseData: [
            //     { "id": 1, "name": "Overview" },
            //     { "id": 2, "name": "Curriculum" },
            //     { "id": 3, "name": "Review" },
            //     { "id": 4, "name": "Related Course" }
            // ],
            // subCourseText: ["#017cb2", "#2a2a2a", , "#2a2a2a", , "#2a2a2a"],
            // likeFlag: false,
            // rating: [1, 2, 3, 4, 5]
        }
    }

    componentDidMount() {

    }



    render() {
        return (
            <>
                <StatusBar barStyle="dark-content" backgroundColor={Colors.statusBarBgColor} />
                <>
                    <HeaderScreen
                        headerLeft={"Favourite"}
                        onPress={() => this.props.navigation.navigate("CourseList")}
                    />
                    <View style={styles.container}>

                        <View style={styles.header_container} />
                        <View style={{ flex: 0.9 }}>
                            <ScrollView >
                                <View style={styles.main_view}>
                                    <Text style={styles.main_text}>Popular Sub Course</Text>

                                    <FlatList
                                        data={this.state.data}
                                        renderItem={({ item }) => <CourseListCell handleCourseList={() => this.handleCourseList()} />}
                                        keyExtractor={item => item.id}
                                    />
                                </View>
                            </ScrollView>
                        </View>
                        <Footer
                            handleHome={() => this.handleHome()}
                            handleCard={() => this.handleCard()}
                            handleSearch={() => this.handleSearch()}
                            flagForMessage={true}
                        />
                    </View>
                </>
            </>
        );
    }
};

const styles = StyleSheet.create({
    container: { flex: 1 },
    header_container: { height: 15, backgroundColor: Colors.headerColor, borderBottomLeftRadius: 50, borderBottomRightRadius: 50 },
    main_view: { paddingHorizontal: 20, marginTop: 20, backgroundColor: Colors.white },
    main_text: { color: "#2a2a2a", fontSize: fontComponent.large_size, fontFamily: fontFamilyComponent.Muli_Bold, paddingVertical: 15 },

});

export default FavouriteComponent;