
import React, { Component } from 'react';
import {
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Image,
    Dimensions,
    TouchableOpacity,
    ImageBackground,
    FlatList
} from 'react-native';
import Colors from "../../Utils/Colors";
const screenHeight = Dimensions.get('window').height;
const screenWidth = Dimensions.get('window').width;
import fontComponent from "../../Utils/fonts";
import fontFamilyComponent from "../../Utils/fontFaimly";
import ProductListComponent from "./../../Components/Course/ProductList"
const product01Icon = require("./../../Images/Product/product01.png")
const product02Icon = require("../../Images/Product/product02.png")
const product03Icon = require("./../../Images/Product/product03.png")
const product04Icon = require("./../../Images/Product/product04.png")
const logoIcon = require("./../../Images/Product/logo.png")

class HomeComponent extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            data: [{ "image": product01Icon }, {"image": product02Icon}, {"image": product03Icon}, {"image": product04Icon}]
        }
    }

    render() {
        return (
            <View style={{ padding: 20, backgroundColor: Colors.white, height: screenHeight / 1.36 }}>

                <Text style={{ color: "#2a2a2a", fontFamily: fontFamilyComponent.Muli_Bold, fontSize: fontComponent.large_size }}>Classroom Courses</Text>

                <FlatList
                    data={this.state.data}
                    numColumns={2}
                    renderItem={({ item }) =>
                        <View style={{ flexDirection: "row", paddingBottom: 30, paddingRight: 15, justifyContent: "space-between", paddingTop: 20 }}>
                            <ProductListComponent 
                                item = {item}
                            />
                        </View>
                    }
                    keyExtractor={item => item.id}
                />
            </View>

        );
    }
};

const styles = StyleSheet.create({
    container: { backgroundColor: Colors.white, flex: 1 },
    imageLogo: { width: 120, height: 120, borderRadius: 60 },
    logoContainer: { flex: 0.5, justifyContent: "center", alignItems: "center" },
});

export default HomeComponent;