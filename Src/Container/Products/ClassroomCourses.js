
import React, { Component } from 'react';
import {
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Image,
    Dimensions,
    TouchableOpacity,
    ImageBackground
} from 'react-native';
import HeaderScreen from "./../../Components/Header";
import Colors from "./../../Utils/Colors";
const screenHeight = Dimensions.get('window').height;
const screenWidth = Dimensions.get('window').width;
const appLog = require("./../../Images/logo.png");
const ButtonImage = require("./../../Images/Button.png");
import fontComponent from "./../../Utils/fonts";
import fontFamilyComponent from "./../../Utils/fontFaimly";
import ClassroomCourseType from "./../Products/ClassroomCoursesType"


const DiscountsIcon = require("./../../Images/Product/Discount.png")
const pdfIcon = require("../../Images/Product/Pdf.png")
const FreeDeliveryIcon = require("./../../Images/Product/Free-Delivery.png")
const ExamIcon = require("./../../Images/Product/Exam.png")
const TeacherIcon = require("./../../Images/Product/Teacher.png")
const PdfSubIcon = require("./../../Images/Product/Pdf-icon.png")
const OfferIcon = require("./../../Images/Product/Offer.png")
const logistics = require("./../../Images/Product/logistics.png")

class HomeComponent extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            flag: false
        }
    }


    handleListCell = () => {
        if (this.state.flag == false) {
            this.setState({ flag: true })
        }
    }

    render() {
        return (
            <>
            {this.state.flag == false?
                <View style={{ padding: 20 }}>
                    {/* {this.state.flag ? <ClassroomCourseType /> : null} */}
                    <Text style={{ color: "#2a2a2a", fontFamily: fontFamilyComponent.Muli_Bold, fontSize: fontComponent.large_size }}>Classroom Courses</Text>


                    <View style={{ flexDirection: "row", justifyContent: "space-between", paddingTop: 20 }}>

                        <TouchableOpacity
                            onPress={() => this.handleListCell()}
                            style={{ alignSelf: 'center' }}>
                            <View style={{ position: "relative" }}>
                                <Image source={DiscountsIcon} />
                            </View>
                            <View style={{ position: "absolute", padding: 15, alignItems: "center" }}>

                                <Image source={OfferIcon} />
                                <Text style={{ color: Colors.white, textAlign: "center", paddingTop: 10, fontSize: fontComponent.micro_size, fontFamily: fontFamilyComponent.Raleway_Medium }}>Popular Exam Guides Are Available</Text>

                            </View>
                        </TouchableOpacity>

                        <View style={{ alignSelf: 'center' }}>
                            <View style={{ position: "relative" }}>
                                <Image source={FreeDeliveryIcon} />
                            </View>
                            <View style={{ position: "absolute", padding: 15, alignItems: "center" }}>

                                <Image source={logistics} />
                                <Text style={{ color: Colors.white, textAlign: "center", paddingTop: 10, fontSize: fontComponent.micro_size, fontFamily: fontFamilyComponent.Raleway_Medium }}>Popular Exam Guides Are Available</Text>

                            </View>
                        </View>


                    </View>


                    <View style={{ flexDirection: "row", justifyContent: "space-between", paddingTop: 20 }}>

                        <View style={{ alignSelf: 'center' }}>
                            <View style={{ position: "relative" }}>
                                <Image source={pdfIcon} />
                            </View>
                            <View style={{ position: "absolute", padding: 15, alignItems: "center" }}>

                                <Image source={PdfSubIcon} />
                                <Text style={{ color: Colors.white, textAlign: "center", paddingTop: 10, fontSize: fontComponent.micro_size, fontFamily: fontFamilyComponent.Raleway_Medium }}>Popular Exam Guides Are Available</Text>

                            </View>
                        </View>

                        <View style={{ alignSelf: 'center' }}>
                            <View style={{ position: "relative" }}>
                                <Image source={ExamIcon} />
                            </View>
                            <View style={{ position: "absolute", padding: 15, alignItems: "center" }}>

                                <Image source={TeacherIcon} />
                                <Text style={{ color: Colors.white, textAlign: "center", paddingTop: 10, fontSize: fontComponent.micro_size, fontFamily: fontFamilyComponent.Raleway_Medium }}>Popular Exam Guides Are Available</Text>

                            </View>
                        </View>
                    </View>
                </View>
                :
                
                <ClassroomCourseType /> }
            </>
        );
    }
};

const styles = StyleSheet.create({
    container: { backgroundColor: Colors.white, flex: 1 },
    imageLogo: { width: 120, height: 120, borderRadius: 60 },
    logoContainer: { flex: 0.5, justifyContent: "center", alignItems: "center" },
});

export default HomeComponent;