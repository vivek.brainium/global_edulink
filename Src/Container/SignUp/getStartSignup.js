
import React, { Component } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Image,
    Dimensions,
    ImageBackground
} from 'react-native';
import HeaderScreen from "./../../Components/Header";
import Colors from "./../../Utils/Colors";
import TextField from "./../../Components/Comman/TextComponent"
import Fontisto from 'react-native-vector-icons/Fontisto';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import AntDesign from 'react-native-vector-icons/AntDesign';
const TestImage = require("./../../Images/Testing/logo.png");
const screenHeight = Dimensions.get('window').height;
const screenWidth = Dimensions.get('window').width;
import Ionicons from 'react-native-vector-icons/Ionicons';
import { TouchableOpacity } from 'react-native-gesture-handler';
const appLog = require("./../../Images/logo.png");
const ButtonImage = require("./../../Images/Button.png")
import fontComponent from "./../../Utils/fonts"
import fontFamilyComponent from "./../../Utils/fontFaimly"
const signUpLogo = require("./../../Images/signUp.png");
import ButtonComponent from "./../../Components/Button"

class GetStartSignUp extends React.Component {

    render() {
        return (
            <>
                <StatusBar barStyle="dark-content" backgroundColor={Colors.statusBarBgColor} />
                <ScrollView>
                    <View style={styles.container}>
                        <View style={styles.image_container}>
                            <Image
                                source={signUpLogo}
                                style={styles.image_style}
                            />
                        </View>

                        <View style={styles.text_style}>
                            <View style={styles.getStart}>
                                <Text style={styles.eduTextStyles}>Global Edulink is a higher</Text>
                                <Text style={styles.educationText}>education company</Text>
                                <Text style={styles.descText}>We deliver hundreds of courses under the defferent education discipline via Online. Classroom and Live online.</Text>
                            </View>

                            <View style={styles.buttonContainer}>
                                <ButtonComponent
                                    imageStyle={styles.buttomImage}
                                    buttomTitle={"Get Started"}
                                />
                            </View>

                            <View style={styles.skipButton}>
                                <TouchableOpacity
                                    activeOpacity={.5}
                                    style={styles.buttonSubCont}>
                                    <Text style={styles.skipTextStyle}>Skip</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </>
        )
    }
}

export default GetStartSignUp;

const styles = StyleSheet.create({
    container: { flex: 1, alignItems: "center", backgroundColor: Colors.white },
    buttomText: { color: Colors.white, fontSize: fontComponent.large_size, fontFamily: fontFamilyComponent.Raleway_Medium },
    buttomImage: { height: screenHeight / 10, width: screenWidth / 1.5 },
    image_style: { width: screenWidth / 1.2, height: screenHeight / 2.8 },
    text_style: { paddingHorizontal: 25, flex: 0.6 },
    getStart: { paddingTop: 40, alignItems: "center" },
    buttonContainer: { paddingTop: 30, alignItems: "center" },
    skipButton: { alignItems: "center", marginTop: 25 },
    skipTextStyle: { color: "#001f43", fontFamily: fontFamilyComponent.Raleway_Medium, fontSize: fontComponent.large_size },
    buttonSubCont: { height: screenHeight / 10, width: screenWidth / 1.5, alignItems: "center", justifyContent: "center", borderWidth: 1, borderRadius: 50 },
    educationText: { color: Colors.educationColor, fontSize: fontComponent.extra_large, fontFamily: fontFamilyComponent.Raleway_Bold, paddingTop: 5 },
    descText: { color: "#68859b", fontSize: fontComponent.small_size, fontFamily: fontFamilyComponent.Raleway_Regular, paddingTop: 15, textAlign: "center" },
    eduTextStyles: { color: Colors.getStart, fontSize: fontComponent.medium_size, fontFamily: fontFamilyComponent.Raleway_Medium },
    buttomStyle: { justifyContent: "center", alignItems: "center", height: screenHeight / 11.5 },
    image_container: { flex: 0.4, width: screenWidth, backgroundColor: Colors.headerColor, alignItems: "center", justifyContent: "center" }
});