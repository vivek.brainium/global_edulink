
import React, { Component } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Image,
    Dimensions,
    ImageBackground
} from 'react-native';
import HeaderScreen from "./../../Components/Header";
import Colors from "./../../Utils/Colors";
import TextField from "./../../Components/Comman/TextComponent"
import Fontisto from 'react-native-vector-icons/Fontisto';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import AntDesign from 'react-native-vector-icons/AntDesign';
const TestImage = require("./../../Images/Testing/logo.png");
const screenHeight = Dimensions.get('window').height;
const screenWidth = Dimensions.get('window').width;
import Ionicons from 'react-native-vector-icons/Ionicons';
import { TouchableOpacity } from 'react-native-gesture-handler';
const appLog = require("./../../Images/logo.png");
const ButtonImage = require("./../../Images/Button.png")
import fontComponent from "./../../Utils/fonts"
import fontFamilyComponent from "./../../Utils/fontFaimly"
import CheckBox from 'react-native-check-box'
import ButtonComponent from "./../../Components/Button"


class Forgot extends React.Component {
    constructor(props) {
        super(props)
        this.state = {

        }
    }

    handleChangeIcon = () => {
        if (this.state.passwordFlag) {
            this.setState({
                passwordFlag: false
            })
        } else {
            this.setState({
                passwordFlag: true
            })
        }
    }

    render() {
        return (
            <>
                <StatusBar barStyle="dark-content" backgroundColor={Colors.statusBarBgColor} />
                <>
                    <HeaderScreen
                        headerLeft={""}
                    />
                    <View style={styles.container}>
                        <View style={styles.sub_container}>
                            <Image
                                source={appLog}
                                style={styles.imageLogo}
                            />
                        </View>

                        <View style={{ alignItems: "center" }}>
                            <Text style={{ color: "#373737", fontFamily: fontFamilyComponent.Raleway_Medium, fontSize: fontComponent.large_size }}>Forgot your password ?</Text>
                            <Text style={{ color: "#6e7578", textAlign: "center", paddingTop: 15, fontFamily: fontFamilyComponent.Raleway_Medium, fontSize: fontComponent.small_size }}>Enter your email address below to reset password</Text>
                        </View>

                        <View style={{ flex: 0.13, paddingTop: 20 }}>
                            <TextField
                                // onChangeText={()=> this}
                                placeholder={"Email address"}
                                secureTextEntry={false}
                                icon={true}
                                iconName={Fontisto}
                                iconType={"email"}
                                iconColor={Colors.headerColor}
                                size={25}
                                iconStyle={{ paddingHorizontal: 5 }}
                            />
                        </View>

                        <View style={styles.buttonContainer}>
                                <ButtonComponent
                                    imageStyle={styles.buttomImage}
                                    buttomTitle={"Get Started"}
                                />
                            </View>

                    </View>
                </>
            </>
        );
    }
};

const styles = StyleSheet.create({
    container: { paddingHorizontal: 25, backgroundColor: Colors.white, flex: 1 },
    sub_container: { flex: 0.4, justifyContent: "center", alignItems: "center" },
    imageLogo: { width: 120, height: 120, borderRadius: 60 },
    buttomImage: { height: screenHeight / 10, width: screenWidth / 1.5 },
    buttonContainer: { flex: 0.4, justifyContent: "center", alignItems: "center" }
});

export default Forgot;