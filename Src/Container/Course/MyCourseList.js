
import React, { Component } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Image,
    Dimensions,
    ImageBackground,
    FlatList
} from 'react-native';
import HeaderScreen from "../../Components/Header";
import Colors from "../../Utils/Colors";
const screenHeight = Dimensions.get('window').height;
const screenWidth = Dimensions.get('window').width;
import { TouchableOpacity } from 'react-native-gesture-handler';
import fontComponent from "../../Utils/fonts"
import fontFamilyComponent from "../../Utils/fontFaimly"
import TabViewComponent from "../../Components/Comman/TabView"
import Footer from "../../Components/Comman/Footer"
const userIcon = require("./../../Images/User.png")
import CourseListCell from "../../Components/Course/CourseListCell"

class CourseList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            tabBackgroundColor: Colors.tabColor,
            tabBackgroundColor1: Colors.headerColor,
            tabBackgroundColor2: Colors.headerColor,
            tabBackgroundColor3: Colors.headerColor
        }
    }




    handleTabView = (val) => {
        if (val == "1") {
            this.setState({
                tabBackgroundColor: Colors.tabColor,
                tabBackgroundColor1: Colors.headerColor,
                tabBackgroundColor2: Colors.headerColor,
                tabBackgroundColor3: Colors.headerColo
            })
        } else if (val == "2") {
            this.setState({
                tabBackgroundColor: Colors.headerColor,
                tabBackgroundColor1: Colors.tabColor,
                tabBackgroundColor2: Colors.headerColor,
                tabBackgroundColor3: Colors.headerColo
            })
        } else if (val == "3") {
            this.setState({
                tabBackgroundColor: Colors.headerColor,
                tabBackgroundColor1: Colors.headerColor,
                tabBackgroundColor2: Colors.tabColor,
                tabBackgroundColor3: Colors.headerColo
            })
        } else if (val == "4") {
            this.setState({
                tabBackgroundColor: Colors.headerColor,
                tabBackgroundColor1: Colors.headerColor,
                tabBackgroundColor2: Colors.headerColor,
                tabBackgroundColor3: Colors.tabColor
            })
        }
    }

    handleCard() {

    }
    handleSearch() {

    }
    handleHome() {

    }

    render() {
        const DATA = [1, 2, 3, 4, 5, 6]
        return (
            <>
                <StatusBar barStyle="dark-content" backgroundColor={Colors.statusBarBgColor} />
                <>
                    <HeaderScreen
                        headerLeft={"Home"}
                    />
                    <View style={styles.container}>

                        <TabViewComponent
                            handleTabView={(val) => this.handleTabView(val)}
                            tabBackgroundColor={this.state.tabBackgroundColor}
                            tabBackgroundColor1={this.state.tabBackgroundColor1}
                            tabBackgroundColor2={this.state.tabBackgroundColor2}
                            tabBackgroundColor3={this.state.tabBackgroundColor3}
                        />


                        <View style={{ flex: 1, paddingHorizontal: 25, backgroundColor: Colors.white }}>
                            <Text style={{ color: Colors.black, fontFamily: fontFamilyComponent.Raleway_Bold, fontSize: fontComponent.large_size, paddingVertical: 10 }}>Popular Course Title</Text>

                            <FlatList
                                data={DATA}
                                renderItem={({ item }) => <CourseListCell />}
                                keyExtractor={item => item.id}
                            />



                        </View>
                        <Footer
                            handleHome={() => this.handleHome()}
                            handleCard={() => this.handleCard()}
                            handleSearch={() => this.handleSearch()}
                        />
                    </View>
                </>
            </>
        );
    }
};

const styles = StyleSheet.create({
    container: { backgroundColor: Colors.white, flex: 1 },
    imageLogo: { width: 120, height: 120, borderRadius: 60 },
    logoContainer: { flex: 0.5, justifyContent: "center", alignItems: "center" },
});

export default CourseList;