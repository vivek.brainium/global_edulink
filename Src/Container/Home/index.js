
import React, { Component } from 'react';
import {
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Image,
    Dimensions,
    TouchableOpacity,
    ImageBackground
} from 'react-native';
import HeaderScreen from "./../../Components/Header";
import Colors from "./../../Utils/Colors";
const screenHeight = Dimensions.get('window').height;
const screenWidth = Dimensions.get('window').width;
const appLog = require("./../../Images/logo.png");
const ButtonImage = require("./../../Images/Button.png");
import fontComponent from "./../../Utils/fonts";
import fontFamilyComponent from "./../../Utils/fontFaimly";
import TabViewComponent from "./../../Components/Comman/TabView";
import Footer from "./../../Components/Comman/Footer";
import ClassroomCoursesComponent from "./../Products/ClassroomCourses"
import FreebiesComponents from "./../Freebies"
import CareerSupport from "./../Career/index"

class HomeComponent extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            tabBackgroundColor: Colors.tabColor,
            tabBackgroundColor1: Colors.headerColor,
            tabBackgroundColor2: Colors.headerColor,
            tabBackgroundColor3: Colors.headerColor,
            courseFlag: false,
            tabChange: "1"
        }
    }




    handleTabView = (val) => {
        this.setState({
            tabChange: val
        })
        if (val == "1") {
            this.setState({
                tabBackgroundColor: Colors.tabColor,
                tabBackgroundColor1: Colors.headerColor,
                tabBackgroundColor2: Colors.headerColor,
                tabBackgroundColor3: Colors.headerColo
            })
        } else if (val == "2") {
            this.setState({
                tabBackgroundColor: Colors.headerColor,
                tabBackgroundColor1: Colors.tabColor,
                tabBackgroundColor2: Colors.headerColor,
                tabBackgroundColor3: Colors.headerColo
            })
        } else if (val == "3") {
            this.setState({
                tabBackgroundColor: Colors.headerColor,
                tabBackgroundColor1: Colors.headerColor,
                tabBackgroundColor2: Colors.tabColor,
                tabBackgroundColor3: Colors.headerColo
            })
        } else if (val == "4") {
            this.setState({
                tabBackgroundColor: Colors.headerColor,
                tabBackgroundColor1: Colors.headerColor,
                tabBackgroundColor2: Colors.headerColor,
                tabBackgroundColor3: Colors.tabColor
            })
        }
    }

    handleCard() {

    }
    handleSearch() {

    }
    handleHome() {

    }

    handleServices() {

    }

    handleCourses() {
        this.props.navigation.navigate("CourseList")
    }

    render() {
        return (
            <>
                <StatusBar barStyle="dark-content" backgroundColor={Colors.statusBarBgColor} />
                <>
                    <HeaderScreen
                        headerLeft={"Home"}
                    />
                    <View style={styles.container}>

                        <TabViewComponent
                            handleTabView={(val) => this.handleTabView(val)}
                            tabBackgroundColor={this.state.tabBackgroundColor}
                            tabBackgroundColor1={this.state.tabBackgroundColor1}
                            tabBackgroundColor2={this.state.tabBackgroundColor2}
                            tabBackgroundColor3={this.state.tabBackgroundColor3}
                        />




                        {(this.state.tabChange == "1") &&

                            <View style={{ flex: 1 }}>
                                <View style={styles.logoContainer}>
                                    <Image
                                        source={appLog}
                                        style={styles.imageLogo}
                                    />
                                </View>
                                <View style={{ alignItems: "center", flex: 0.2, justifyContent: "center" }}>
                                    <TouchableOpacity
                                        onPress={() => this.handleServices()}
                                        activeOpacity={.6}
                                        style={{ borderRadius: 50, justifyContent: "center", alignItems: "center", backgroundColor: "#ffb606", width: screenWidth / 1.4, height: screenHeight / 12 }}
                                    >
                                        <Text style={{ color: Colors.white, fontSize: fontComponent.large_size, fontFamily: fontFamilyComponent.Raleway_SemiBold }}>Services</Text>
                                    </TouchableOpacity>


                                    <TouchableOpacity
                                        onPress={() => this.handleCourses()}
                                        activeOpacity={.6}
                                        style={{ borderRadius: 50, marginTop: 30, justifyContent: "center", alignItems: "center", backgroundColor: "#98a8b1", width: screenWidth / 1.4, height: screenHeight / 12 }}
                                    >
                                        <Text style={{ color: Colors.white, fontSize: fontComponent.large_size, fontFamily: fontFamilyComponent.Raleway_SemiBold }}>Courses</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        }

                        {this.state.tabChange == "2" &&
                            <View style={{ flex: 0.8, backgroundColor: Colors.white }}>
                                <ClassroomCoursesComponent />
                            </View>
                        }


                        {this.state.tabChange == "3" &&
                            <View style={{ flex: 0.89, backgroundColor: Colors.white }}>
                                <CareerSupport />
                            </View>
                        }

                        {this.state.tabChange == "4" &&
                            <View style={{ flex: 0.8, backgroundColor: Colors.white }}>
                                <FreebiesComponents />
                            </View>
                        }


                        <Footer
                            handleHome={() => this.handleHome()}
                            handleCard={() => this.handleCard()}
                            handleSearch={() => this.handleSearch()}
                        />
                    </View>
                </>
            </>
        );
    }
};

const styles = StyleSheet.create({
    container: { backgroundColor: Colors.white, flex: 1 },
    imageLogo: { width: 120, height: 120, borderRadius: 60 },
    logoContainer: { flex: 0.5, justifyContent: "center", alignItems: "center" },
});

export default HomeComponent;